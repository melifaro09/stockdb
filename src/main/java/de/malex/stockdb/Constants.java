package de.malex.stockdb;

/**
 * Global constants
 * 
 * @author Alexandr Mitiaev
 */
public class Constants {
	/**
	 * English locale
	 */
	public static final String LOCALE_EN		=			"en";
	
	/**
	 * German locale
	 */
	public static final String LOCALE_DE		=			"de";
	
	/**
	 * Russian locale
	 */
	public static final String LOCALE_RU		=			"ru";
	
	/**
	 * Locale key string for preferences
	 */
	public static final String PREFS_LOCALE		=			"locale";
}
