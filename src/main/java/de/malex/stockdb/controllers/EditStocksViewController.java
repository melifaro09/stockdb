package de.malex.stockdb.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.prefs.Preferences;

import de.malex.stockdb.Main;
import de.malex.stockdb.db.DBFacade;
import de.malex.stockdb.db.DatabaseException;
import de.malex.stockdb.db.model.Stock;
import de.malex.stockdb.financial.providers.FinanceService;
import de.malex.stockdb.financial.providers.IProvider;
import de.malex.stockdb.financial.providers.ProviderException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

/**
 * The controller for the ProgressView.fxml form
 * 
 * @author Alexandr Mitiaev
 */
public class EditStocksViewController implements Initializable {
	/**
	 * Preference name of "provider index" value
	 */
	private static final String PREFS_PROVIDER_IDX			=			"edit_form_provider_index";
	
	/**
	 * Window stage
	 */
	private Stage stage;
	
	/**
	 * The {@link TableView} to display stock list
	 */
	@FXML
	private TableView<Stock> tblStocks;
	
	/**
	 * Stock name to search
	 */
	@FXML
	private TextField edName;
	
	/**
	 * Label to display search stock result
	 */
	@FXML
	private Label lbSearchResult;
	
	/**
	 * Button to append search result
	 */
	@FXML
	private Button btnAppend;
	
	/**
	 * Button to delete stock from DB
	 */
	@FXML
	private Button btnDelete;
	
	/**
	 * Data providers
	 */
	@FXML
	private ComboBox<String> cbProvider;
	
	/**
	 * The founded {@link Stock} instance
	 */
	@FXML
	private Stock searchResult = null;
	
	/**
	 * Observable stocks list
	 */
	private ObservableList<Stock> stocks;
	
	/**
	 * Table field
	 */
	@FXML
	private TextField edFilter;
	
	/**
	 * Custom stock symbol/ISIN
	 */
	@FXML
	private TextField edCustomSymbol;
	
	/**
	 * Custom stock name
	 */
	@FXML
	private TextField edCustomName;
	
	/**
	 * Price for custom added stock
	 */
	@FXML
	private TextField edCustomPrice;
	
	/**
	 * Sound for success result
	 */
	private final AudioClip soundFounded = new AudioClip(getClass().getResource("sounds/founded.mp3").toExternalForm());
	
	/**
	 * Sound for error result
	 */
	private final AudioClip soundNotFounded = new AudioClip(getClass().getResource("sounds/error.mp3").toExternalForm());

	/**
	 * Initialize form
	 */
	public void initialize(URL location, ResourceBundle resources) {
	}
	
	/**
	 * Set up views stage
	 * 
	 * @param stage The {@link Stage}
	 */
	public void init(Stage stage) {
		this.stage = stage;
		
		loadProviders();
		initTableView();
	}
	
	/**
	 * Load list of data providers
	 */
	private void loadProviders() {
		final Preferences prefs = Main.getPrefs();
		
		cbProvider.getItems().clear();
		
		for (final IProvider provider: FinanceService.getInst().getProviders()) {
			cbProvider.getItems().add(provider.getName());
		}
		cbProvider.getSelectionModel().select(prefs.getInt(PREFS_PROVIDER_IDX, 0));
		
		cbProvider.valueProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				prefs.putInt(PREFS_PROVIDER_IDX, cbProvider.getSelectionModel().getSelectedIndex());
			}
		});
	}
	
	/**
	 * Initialize the {@link #edFilter} component
	 */
	private void initStockFilter() {
		final FilteredList<Stock> filteredData = new FilteredList<Stock>(stocks, new Predicate<Stock>() {
			public boolean test(Stock s) {
				return true;
			}
		});
		
		edFilter.textProperty().addListener(new ChangeListener<String>() {
			public void changed(final ObservableValue<? extends String> observable, final String oldValue, final String newValue) {
				filteredData.setPredicate(new Predicate<Stock>() {
					public boolean test(Stock stock) {
					        if (newValue == null || newValue.isEmpty()) {
					            return true;
					        }
					        
					        final String lowerCaseFilter = newValue.toLowerCase();
					        
					        if (stock.getName().toLowerCase().contains(lowerCaseFilter)) {
					            return true;
					        } else if (stock.getSymbol().toLowerCase().contains(lowerCaseFilter)) {
					            return true;
					        }
					        return false;
					    }
				});
			    }
		});
		
		// Wrap the FilteredList in a SortedList. 
        final SortedList<Stock> sortedData = new SortedList<Stock>(filteredData);
        sortedData.comparatorProperty().bind(tblStocks.comparatorProperty());
        tblStocks.setItems(sortedData);
	}
	
	/**
	 * Initialize {@link TableView} to display 
	 */
	@SuppressWarnings("unchecked")
	private void initTableView() {
		TableColumn<Stock, String> colSymbol = new TableColumn<Stock, String>("Symbol");
		colSymbol.setCellValueFactory(new PropertyValueFactory<Stock, String>("symbol"));
		colSymbol.setPrefWidth(60);
		
		TableColumn<Stock, String> colName = new TableColumn<Stock, String>("Name");
		colName.setCellValueFactory(new PropertyValueFactory<Stock, String>("name"));
		colName.setPrefWidth(250);
		
		TableColumn<Stock, Double> colPrice = new TableColumn<Stock, Double>("Price");
		colPrice.setCellValueFactory(new PropertyValueFactory<Stock, Double>("actualPrice"));
		colPrice.setPrefWidth(100);

		tblStocks.getColumns().clear();
		tblStocks.getColumns().addAll(colSymbol, colName, colPrice);
		
		stocks = FXCollections.observableArrayList();
		try {
			stocks.addAll(DBFacade.getAllStocks());
		} catch (DatabaseException e) {
			Main.showAlert(AlertType.ERROR, "Error", "Database error", e.getMessage());
		}
		initStockFilter();
		//tblStocks.setItems(stocks);
		
		tblStocks.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Stock>() {
			public void changed(ObservableValue<? extends Stock> obs, Stock oldSelection, Stock newSelection) {
				btnDelete.setDisable(newSelection == null);
			}
		});
		
		tblStocks.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
	}
	
	/**
	 * Update stock list
	 * 
	 * @throws DatabaseException
	 */
	public void updateStocks() {
		try {
			stocks.clear();
			stocks.addAll(DBFacade.getAllStocks());
		} catch (Exception e) {
			Main.showAlert(AlertType.ERROR, "Error", "Error loading stocks from database", e.getMessage());
		}
	}
	
	/**
	 * Save button clicked
	 */
	@FXML
	public void onCloseClicked() {
		stage.close();
	}
	
	/**
	 * Search stock by given name
	 */
	@FXML
	public void onSearchStockClicked() {
		if (edName.getText().isEmpty())
			return;
		
		try {
			final IProvider provider = FinanceService.getInst().getProvider(cbProvider.getSelectionModel().getSelectedIndex());
			
			searchResult = provider.search(edName.getText());

			if (searchResult != null) {
				soundFounded.play();
				
				lbSearchResult.setTooltip(new Tooltip(String.format("%s\n%s\n%.2f %s", searchResult.getSymbol(), searchResult.getName(), searchResult.getActualPrice(), searchResult.getCurrency())));
				lbSearchResult.setText(String.format("Founded: %s, %s", searchResult.getSymbol(), searchResult.getName()));
				lbSearchResult.setTextFill(Paint.valueOf("green"));
				btnAppend.setDisable(false);
			} else {
				soundNotFounded.play();
				
				lbSearchResult.setText("Stock not found");
				lbSearchResult.setTextFill(Paint.valueOf("red"));
				btnAppend.setDisable(true);
			}
		} catch (ProviderException e) {
			searchResult = null;
			Main.showAlert(AlertType.ERROR, "Error", "Error by getting stock data", "Error searching '" + edName.getText() + "'");
		}
	}
	
	/**
	 * Add founded stock to the database
	 */
	@FXML
	public void onAppendClicked() {
		try {
			DBFacade.saveStock(searchResult);
			updateStocks();
		} catch (Exception e) {
			Main.showAlert(AlertType.ERROR, "Error", "Saving error", e.getMessage());
		}
	}
	
	/**
	 * Append custom stock
	 */
	@FXML
	public void onCustomAppendClicked() {
		if (edCustomSymbol.getText().isEmpty()) {
			Main.showAlert(AlertType.WARNING, "Add stock", "Symbol/ISIN of stock is required to adding", "Enter stock symbol/ISIN");
			edCustomSymbol.requestFocus();
			return;
		}
		
		if (edCustomName.getText().isEmpty()) {
			Main.showAlert(AlertType.WARNING, "Add stock", "Name/description of stock is required to adding", "Enter stock name/description");
			edCustomName.requestFocus();
			return;
		}
		
		if (edCustomPrice.getText().isEmpty()) {
			Main.showAlert(AlertType.WARNING, "Add stock", "Price of stock is required to adding", "Enter stock price");
			edCustomPrice.requestFocus();
			return;
		}

		try {
			final Stock stock = new Stock();
			stock.setSymbol(edCustomSymbol.getText());
			stock.setName(edCustomName.getText());
			
			try {
				stock.setActualPrice(Double.parseDouble(edCustomPrice.getText().replace(",", ".")));
			} catch (Exception e) {
				Main.showAlert(AlertType.ERROR, "Error", "Invalid price value", "Check price value");
				edCustomPrice.requestFocus();
				return;
			}
			
			DBFacade.saveStock(stock);
			
			edCustomName.setText("");
			edCustomSymbol.setText("");
			edCustomPrice.setText("");
			
			updateStocks();
			
			Main.showAlert(AlertType.INFORMATION, "Add stock", "Stock added", "Stock successfully added to the database");
		} catch (DatabaseException e) {
			Main.showAlert(AlertType.ERROR, "Error", "Error by saving stock in database", e.getMessage());
		}
	}
	
	/**
	 * Delete selected stock from database
	 */
	@FXML
	public void onDeleteClicked() {
		if (Main.confirmDialog("Delete stock", "Delete all selected stocks from the database?")) {
			
			for (final Stock stock: tblStocks.getSelectionModel().getSelectedItems()) {
				try {
					DBFacade.deleteStock(stock);
				} catch (DatabaseException e) {
					Main.showAlert(AlertType.ERROR, "Error", "Delete error", e.getMessage());
				}
			}
			
			updateStocks();
		}
	}
}
