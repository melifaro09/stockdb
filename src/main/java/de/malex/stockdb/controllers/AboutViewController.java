package de.malex.stockdb.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 * The controller for the SettingsView.fxml view
 * 
 * @author Alexandr Mitiaev
 *
 */
public class AboutViewController implements Initializable {
	/**
	 * The views stage
	 */
	private Stage stage;

	/**
	 * Create new {@link AboutViewController} instance
	 */
	public AboutViewController() {
	}

	/**
	 * Initialize view
	 */
	public void initialize(URL location, ResourceBundle resources) {
	}
	
	/**
	 * Set up views stage
	 * 
	 * @param stage The {@link Stage}
	 */
	public void init(Stage stage) {
		this.stage = stage;
	}
	
	/**
	 * Close about window
	 */
	@FXML
	public void onCloseClicked() {
		stage.close();
	}
}
