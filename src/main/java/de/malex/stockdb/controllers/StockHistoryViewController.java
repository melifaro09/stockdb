package de.malex.stockdb.controllers;

import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.ResourceBundle;

import de.malex.stockdb.db.model.Stock;
import de.malex.stockdb.Main;
import de.malex.stockdb.db.model.StockSell;
import de.malex.stockdb.db.model.StockBuy;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * The controller for the ProgressView.fxml form
 * 
 * @author Aidai Kazakbaeva
 */
public class StockHistoryViewController implements Initializable {
	/**
	 * 
	 * @author aidai
	 *
	 */
	enum EOrderType {ESellOrder, EBuyOrder};
	
	/**
	 * Sell image
	 */
	private final Image sellImage = new Image(getClass().getResourceAsStream("/de/malex/stockdb/images/sell.png"));
	
	/**
	 * Buy image
	 */
	private final Image buyImage = new Image(getClass().getResourceAsStream("/de/malex/stockdb/images/buy.png"));
	
	/**
	 * Window stage
	 */
	private Stage stage;
	
	/**
	 * List of users
	 */
	private ObservableList<DisplayedStock> stocks;
	
	/**
	 * List of users
	 */
	@FXML
	private TableView<DisplayedStock> tblStocks;
	
	/**
	 * Stock Title
	 */
	@FXML
	private Label StockHistoryTitle;
	
	/**
	 * Initialize form
	 */
	public void initialize(URL location, ResourceBundle resources) {
		stocks = FXCollections.observableArrayList();
		try {
			initStockTable();
		} catch (Exception e) {
			Main.showAlert(AlertType.ERROR, "Error", "Error table initialization", e.getMessage());
		}
	}
	
	/**
	 * Set up views stage
	 * 
	 * @param stage The {@link Stage}
	 */
	public void init(Stage stage) {
		this.stage = stage;
	}
	
	public void setStock(Stock stock) {
		StockHistoryTitle.setText(stock.getName());
		stocks.clear();
		
		List<StockSell> sellOrders = stock.getSellOrders();
		List<StockBuy> buyOrders = stock.getBuyOrders();
		
		for (StockSell order: sellOrders) {
			DisplayedStock st = new DisplayedStock();
			st.setDate(order.getDateStr());
			st.setNumber(order.getAmount());
			st.setPrice(order.getPrice());
			st.setOrderType(EOrderType.ESellOrder);
			stocks.add(st);
		}
		
		for (StockBuy order: buyOrders) {
			DisplayedStock st = new DisplayedStock();
			st.setDate(order.getDateStr());
			st.setNumber(order.getAmount());
			st.setPrice(order.getPrice());
			st.setOrderType(EOrderType.EBuyOrder);
			stocks.add(st);
		}
		
		tblStocks.refresh();
	}

	
	/**
	 * Initialize import data table
	 * @throws Exception 
	 * @throws ClassNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	private void initStockTable() throws ClassNotFoundException, Exception {
		TableColumn<DisplayedStock, Date> colDate = new TableColumn<DisplayedStock, Date>("Datum");
		colDate.setCellValueFactory(new PropertyValueFactory<DisplayedStock, Date>("date"));
		colDate.setPrefWidth(220);
		
		TableColumn<DisplayedStock, Integer> colAmount = new TableColumn<DisplayedStock, Integer>("Anzahl");
		colAmount.setCellValueFactory(new PropertyValueFactory<DisplayedStock, Integer>("number"));
		colAmount.setPrefWidth(80);

		
		TableColumn<DisplayedStock, Double> colPrice = new TableColumn<DisplayedStock, Double>("Price");
		colPrice.setCellValueFactory(new PropertyValueFactory<DisplayedStock, Double>("price"));
		colPrice.setPrefWidth(100);
		
		
		TableColumn<DisplayedStock, EOrderType> colImage = new TableColumn<DisplayedStock, EOrderType>("");
		colImage.setCellFactory(new Callback<TableColumn<DisplayedStock, EOrderType>,TableCell<DisplayedStock, EOrderType>>(){        
	        @Override
	        public TableCell<DisplayedStock, EOrderType> call(TableColumn<DisplayedStock, EOrderType> param) {
	            final ImageView imageview = new ImageView();
	            imageview.setFitHeight(24);
	            imageview.setFitWidth(24);

	            TableCell<DisplayedStock, EOrderType> cell = new TableCell<DisplayedStock, EOrderType>() {
	            	@Override
	                public void updateItem(EOrderType item, boolean empty) {                        
	                    if (item != null){
	                    	if (item == EOrderType.EBuyOrder) {
	                    		imageview.setImage(buyImage);
	                    	} else {
	                    		imageview.setImage(sellImage);
	                    	}
	                    }
	                }
	            };
	            
	            cell.setGraphic(imageview);
	            return cell;
	        }

	    });
		colImage.setCellValueFactory(new PropertyValueFactory<DisplayedStock, EOrderType>("orderType"));
		
		

		tblStocks.getColumns().clear();
		tblStocks.getColumns().addAll(colImage, colDate, colAmount, colPrice);
		tblStocks.setItems(stocks);
	}
	
	/**
	 * Close form
	 */
	@FXML
	public void onCloseClicked() {
		stage.close();
	}
	
	/**
	 * Class stores stock data to display in a table
	 * 
	 * @author aidai
	 *
	 */
	public class DisplayedStock {
		/**
		 * Order date
		 */
		private String date;

		/**
		 * Number
		 */
		private int number;
		
		/**
		 * Stock price
		 */
		private double price;
		
		/**
		 * Order type
		 */
		private EOrderType orderType;
		
		/**
		 * Create new {@link DisplayedStock} instance
		 */
		public DisplayedStock() {
		}
		
		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}

		public double getPrice() {
			return price;
		}

		public void setPrice(double price) {
			this.price = price;
		}

		public EOrderType getOrderType() {
			return orderType;
		}

		public void setOrderType(EOrderType orderType) {
			this.orderType = orderType;
		}
	}
}
