package de.malex.stockdb.controllers;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import de.malex.stockdb.Main;
import de.malex.stockdb.controllers.readers.ExcelReader;
import de.malex.stockdb.controllers.readers.ImportEntry;
import de.malex.stockdb.db.DBFacade;
import de.malex.stockdb.db.DatabaseException;
import de.malex.stockdb.db.model.Stock;
import de.malex.stockdb.db.model.StockBuy;
import de.malex.stockdb.financial.providers.FinanceService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;


/**
 * The controller for the ProgressView.fxml form
 * 
 * @author Alexandr Mitiaev
 */
public class CSVImportViewController implements Initializable {
	/**
	 * Window stage
	 */
	private Stage stage;
	
	/**
	 * Path to excel file
	 */
	@FXML
	private TextField edExcelFile;
	
	/**
	 * Start import button
	 */
	@FXML
	private Button btnStartImport;
	
	/**
	 * Import table preview
	 */
	@FXML
	private TableView<ImportEntry> tblImport;
	
	/**
	 * List of imported entries
	 */
	private ObservableList<ImportEntry> entries;
	
	/**
	 * Skip first line checkbox
	 */
	@FXML
	private CheckBox cbSkipFirst;

	/**
	 * Initialize form
	 */
	public void initialize(URL location, ResourceBundle resources) {
	}
	
	/**
	 * Set up views stage
	 * 
	 * @param stage The {@link Stage}
	 */
	public void init(Stage stage) {
		this.stage = stage;
	}

	/**
	 * Clear formular
	 */
	public void clearForm() {
		initTable(0);
		edExcelFile.setText("");
		btnStartImport.setDisable(true);
		cbSkipFirst.setSelected(false);
	}

	/**
	 * Initialize table with a given number of fields 
	 * 
	 * @param fieldCount
	 */
	private void initTable(int fieldCount) {
		entries = FXCollections.observableArrayList();
		tblImport.setItems(entries);
		
		tblImport.getColumns().clear();
		for (int i = 0; i < fieldCount; i++) {
			TableColumn<ImportEntry, String> column = new TableColumn<ImportEntry, String>(String.format("Field %d", i + 1));
			column.setCellValueFactory(new PropertyValueFactory<ImportEntry, String>(String.format("field%d", i)));
			column.setPrefWidth(130);
			
			final ComboBox<String> cbType = new ComboBox<>(FXCollections.observableArrayList("-- No import --", "Name", "ISIN/Symbol", "Price", "Price currency", "Number", "Market", "Buy date"));
			cbType.getSelectionModel().select(0);
			column.setGraphic(cbType);
			
			tblImport.getColumns().add(column);
		}
	}
	
	/**
	 * Browse Excel file
	 */
	@FXML
	public void onBrowseClicked() {
		final File inputFile = Main.openFileDialog(stage, "Select Excel file to import", null, Arrays.asList(new ExtensionFilter("MS Excel", "*.xls")));
		
		if (inputFile != null) {
			edExcelFile.setText(inputFile.getAbsolutePath());

			ExcelReader excel = null;
			
			try {
				excel = new ExcelReader(inputFile);
				excel.setActiveSheet(0);
				
				int colCount = 0;
				do {
					try {
						excel.isCellEmpty(colCount, 0);
						colCount++;
					} catch (ArrayIndexOutOfBoundsException e) {
						break;
					}
				} while (true);
				
				initTable(colCount + 1);
				int row = 0;
				do {
					try {
						excel.isCellEmpty(0, row);
						
						final ImportEntry entry = new ImportEntry();
						for (int i = 0; i < colCount; i++) {
							final Field field = entry.getClass().getDeclaredField(String.format("field%d", i));
							field.setAccessible(true);
							field.set(entry, excel.readString(i, row));
						}
						entries.add(entry);
						row++;
					} catch (ArrayIndexOutOfBoundsException e) {
						break;
					}
				} while (true);
				
				restoreSettings();
				btnStartImport.setDisable(false);
			} catch (Exception e) {
				Main.showAlert(AlertType.ERROR, "Error", "Error reading Excel file", e.getMessage());
			} finally {
				if (excel != null) {
					excel.closeDocument();
				}
			}
		}
	}
	
	/**
	 * Save user settings in preferences
	 */
	@SuppressWarnings("unchecked")
	private void saveSettings() {
		final StringBuilder sb = new StringBuilder();
		final Preferences prefs = Main.getPrefs();
		
		for (TableColumn<ImportEntry, ?> column: tblImport.getColumns()) {
			int selected = ((ComboBox<String>)column.getGraphic()).getSelectionModel().getSelectedIndex();
			sb.append(String.format("%d\n", selected));
		}
		
		prefs.put("import_columns", sb.toString());
		prefs.putBoolean("skip_line", cbSkipFirst.isSelected());
	}
	
	/**
	 * Restore user settings from preferences
	 */
	@SuppressWarnings("unchecked")
	private void restoreSettings() {
		final Preferences prefs = Main.getPrefs();
		final String [] values = prefs.get("import_columns", "").split("\n");
		cbSkipFirst.setSelected(prefs.getBoolean("skip_line", true));

		int colIndex = 0;
		for (TableColumn<ImportEntry, ?> column: tblImport.getColumns()) {
			if (values.length > colIndex + 1) {
				((ComboBox<String>)column.getGraphic()).getSelectionModel().select(Integer.parseInt(values[colIndex]));
			}
			colIndex++;
		}
	}
	
	/**
	 * 
	 */
	@FXML
	public void onStartImportClicked() {
		
		int colNameIdx = -1;
		int colISINIdx = -1;
		int colPriceIdx = -1;
		int colPriceCurrencyIdx = -1;
		int colNumber = -1;
		@SuppressWarnings("unused")
		int colMarketIdx = -1;
		int colDateIdx = -1;

		int col = 0;
		for (TableColumn<ImportEntry, ?> column: tblImport.getColumns()) {
			@SuppressWarnings("unchecked")
			int selected = ((ComboBox<String>)column.getGraphic()).getSelectionModel().getSelectedIndex();
			
			switch (selected) {
				case 1: colNameIdx = col; break;
				case 2: colISINIdx = col; break;
				case 3: colPriceIdx = col; break;
				case 4: colPriceCurrencyIdx = col; break;
				case 5: colNumber = col; break;
				case 6: colMarketIdx = col; break;
				case 7: colDateIdx = col; break;
			}
			col++;
		}
		
		if (colNameIdx == -1 || colISINIdx == -1 || colPriceIdx == -1 || colPriceCurrencyIdx == -1 || colDateIdx == -1 || colNumber == -1) {
			Main.showAlert(AlertType.WARNING, "Import error", "Required fields are not filled", "You must specify the columns, containing the values for fields: Name, ISIN, Price, Currency, Date");
			return;
		}
		
		saveSettings();
		
		boolean isFirst = true;
		int totalAdded = 0;
		int totalSkiped = 0;
		int totalErrors = 0;
		for (final ImportEntry entry: entries) {
			if (isFirst && cbSkipFirst.isSelected()) {
				isFirst = false;
				continue;
			}
			isFirst = false;
			
			final String stockISIN = getEntryField(entry, colISINIdx);
			
			if (DBFacade.getStockBySymbol(stockISIN) != null) {
				totalSkiped++;
				continue;
			}

			final Stock stock = new Stock();
			stock.setName(getEntryField(entry, colNameIdx));
			stock.setSymbol(stockISIN);
			stock.setCurrency(getEntryField(entry, colPriceCurrencyIdx));
			stock.setActualPrice(Double.parseDouble(getEntryField(entry, colPriceIdx).replace(",", ".")));
			
			final StockBuy buyOrder = new StockBuy();
			buyOrder.setPrice(Double.parseDouble(getEntryField(entry, colPriceIdx).replace(",", ".")));
			if (!stock.getCurrency().equals("EUR")) {
				try {
					buyOrder.setPrice(buyOrder.getPrice() / FinanceService.getInst().getProvider(0).getExchangeCurse(stock.getCurrency()));
					stock.setActualPrice(buyOrder.getPrice() / FinanceService.getInst().getProvider(0).getExchangeCurse(stock.getCurrency()));
				} catch (Exception e) {
					// Can't get exchange curse: set old values
					buyOrder.setPrice(Double.parseDouble(getEntryField(entry, colPriceIdx).replace(",", ".")));
					stock.setCurrency(getEntryField(entry, colPriceCurrencyIdx));
				}
			}
			try {
				buyOrder.setDate(new SimpleDateFormat("dd.MM.yyyy").parse(getEntryField(entry, colDateIdx)));
			} catch (ParseException e1) {
				buyOrder.setDate(new Date());
			}
			
			String amount = getEntryField(entry, colNumber);
			if (amount.indexOf(".") != -1) {
				amount = amount.substring(0, amount.indexOf("."));
			}
			buyOrder.setAmount(Integer.parseInt(amount));
			
			try {
				DBFacade.saveStock(stock);
				DBFacade.saveStockBuy(buyOrder);

				stock.addBuyOrder(buyOrder);
				DBFacade.saveStock(stock);
				totalAdded++;
			} catch (DatabaseException e) {
				totalErrors++;
			}
		}
		
		Main.showAlert(AlertType.INFORMATION, "Import", "Import done", String.format("Imported: %d\nSkiped: %d\nErrors: %d", totalAdded, totalSkiped, totalErrors));
		stage.close();
	}
	
	/**
	 * Returns value of "fieldXX" field of {@link ImportEntry}
	 * 
	 * @param entry			The {@link ImportEntry} instance
	 * @param fieldIdx		Field index
	 * 
	 * @return				Field value
	 */
	private String getEntryField(final ImportEntry entry, final int fieldIdx) {
		try {
			final Field field = entry.getClass().getDeclaredField(String.format("field%d", fieldIdx));
			return (String) field.get(entry);
		} catch (Exception e) {
			return "";
		}
	}
}
