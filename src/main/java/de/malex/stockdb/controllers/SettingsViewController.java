package de.malex.stockdb.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import de.malex.stockdb.Main;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * The controller for the ProgressView.fxml form
 * 
 * @author Alexandr Mitiaev
 */
public class SettingsViewController implements Initializable {
	/**
	 * Window stage
	 */
	private Stage stage;
	/**
	 * Alpha Vantage API key
	 */
	@FXML
	private TextField edVantageAPI;
	
	/**
	 * World trading data API key
	 */
	@FXML
	private TextField edWorldTradingAPI;
	
	/**
	 * Initialize form
	 */
	public void initialize(URL location, ResourceBundle resources) {
	}
	
	/**
	 * Load settings from preferences
	 */
	private void loadSettings() {
		edVantageAPI.setText(Main.getPrefs().get("vantage_api", ""));
		edWorldTradingAPI.setText(Main.getPrefs().get("worldtrading_api", ""));
	}
	
	/**
	 * Save settings in preferences
	 */
	private void saveSettings() {
		Main.getPrefs().put("vantage_api", edVantageAPI.getText());
		Main.getPrefs().put("worldtrading_api", edWorldTradingAPI.getText());
	}
	
	/**
	 * Returns Alpha Vantage API key
	 * 
	 * @return
	 */
	public String getVantageAPI() {
		return edVantageAPI.getText();
	}
	
	/**
	 * Returns World Trading Data API key
	 * @return
	 */
	public String getWorldTradingAPI() {
		return edWorldTradingAPI.getText();
	}
	
	/**
	 * Set up views stage
	 * 
	 * @param stage The {@link Stage}
	 */
	public void init(Stage stage) {
		this.stage = stage;
		
		loadSettings();
	}
	
	/**
	 * Save settings and close form
	 */
	@FXML
	public void onSaveClicked() {
		saveSettings();
		stage.close();
	}
}
