package de.malex.stockdb.controllers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.prefs.Preferences;

import de.malex.stockdb.AppEventListener;
import de.malex.stockdb.Constants;
import de.malex.stockdb.Main;
import de.malex.stockdb.TimeoutController;
import de.malex.stockdb.TimeoutController.TimeoutException;
import de.malex.stockdb.controllers.readers.ExcelWriter;
import de.malex.stockdb.controllers.readers.ExcelWriter.ExcelDocumentException;
import de.malex.stockdb.db.DBFacade;
import de.malex.stockdb.db.DatabaseException;
import de.malex.stockdb.db.model.Stock;
import de.malex.stockdb.db.model.StockBuy;
import de.malex.stockdb.financial.providers.AlphaVantageProvider;
import de.malex.stockdb.financial.providers.FinanceService;
import de.malex.stockdb.financial.providers.HistoryEntry;
import de.malex.stockdb.financial.providers.IProvider;
import de.malex.stockdb.financial.providers.ProviderException;
import de.malex.stockdb.financial.providers.FinanceService.EHistoryPeriod;
import de.malex.stockdb.financial.providers.WorldTradingDataProvider;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * The controller for MainView.fxml form
 * 
 * @author Alexandr
 */
public class MainViewController implements Initializable, AppEventListener {
	/**
	 * Initial directory
	 */
	public static final String INITIAL_DIR					=			"/";
	
	/**
	 * Preference name of "provider index" value
	 */
	private static final String PREFS_PROVIDER_IDX			=			"main_form_provider_index";
	
	/**
	 * Price update interval: 5 seconds
	 */
	private static final int UPDATE_INTERVAL				=			15000;
	
	/**
	 * Date format for history chart
	 */
	final DateFormat chartDateFormat 						= 			new SimpleDateFormat("dd.MM.yy");
	
	/**
	 * The {@link Stage} of the ProgressView.fxml form
	 */
	private Stage buySellStage = null;
	
	/**
	 * The controller of the ProgressView.fxml form
	 */
	private BuySellViewController buySellController = null;
	
	/**
	 * The {@link Stage} of the StockHistoryView.fxml form
	 */
	private Stage stockHistoryStage = null;
	
	/**
	 * The controller of the StockHistoryView.fxml form
	 */
	private StockHistoryViewController stockHistoryController = null;
	
	/**
	 * The {@link Stage} of the EditStocksView.fxml form
	 */
	private Stage editStocksStage = null;
	
	/**
	 * The controller of the EditStocksView.fxml form
	 */
	private EditStocksViewController editStocksController = null;

	/**
	 * The about view stage
	 */
	private Stage aboutStage = null;
	
	/**
	 * The {@link SettingsViewController} to use
	 */
	private AboutViewController aboutController = null;
	
	/**
	 * The settings view stage
	 */
	private Stage settingsStage = null;
	
	/**
	 * The {@link SettingsViewController} to use
	 */
	private SettingsViewController settingsController = null;
	
	/**
	 * The import view stage
	 */
	private Stage importStage = null;
	
	/**
	 * The {@link CSVImportViewController} to use
	 */
	private CSVImportViewController importController = null;

	/**
	 * "English" menu item
	 */
	@FXML
	private CheckMenuItem mnuLangEn;
	
	/**
	 * "Deutsch" menu item
	 */
	@FXML
	private CheckMenuItem mnuLangDe;
	
	/**
	 * List of users
	 */
	@FXML
	private TableView<Stock> tblStocks;
	
	/**
	 * List of users
	 */
	private ObservableList<Stock> stocks;
	
	/**
	 * The mutex for {@link #stocks}
	 */
	private Object stocksMutex = new Object();
	
	/**
	 * "Sell order" button
	 */
	@FXML
	private Button btnSell;
	
	/**
	 * Stock symbol
	 */
	@FXML
	private Label lbStockSymbol;
	
	/**
	 * Stock name
	 */
	@FXML
	private Label lbStockName;
	
	/**
	 * Stock price
	 */
	@FXML
	private Label lbStockPrice;
	
	/**
	 * The financial data provider
	 */
	@FXML
	private ComboBox<String> cbProvider;
	
	/**
	 * The {@link Timer} to update stocks prices
	 */
	private Timer updateTimer;
	
	/**
	 * History chart
	 */
	@FXML
	private LineChart<String, Number> chartHistory;

	/**
	 * Historical data
	 */
	private ObservableList<Data<String, Number>> historyData;
	
	/**
	 * Chart series
	 */
	private Series<String, Number> historySeries;
	
	/**
	 * History time period
	 */
	@FXML
	private ComboBox<String> cbTimePeriod;
	
	/**
	 * Average sell price label
	 */
	@FXML
	private Label lbAvgSellPrice;
	
	/**
	 * Profit/lost for a last day
	 */
	@FXML
	private Label lbProfitLostDay;
	
	/**
	 * Profit/lost for a last week
	 */
	@FXML
	private Label lbProfitLostWeek;
	
	/**
	 * Profit/lost for a last month
	 */
	@FXML
	private Label lbProfitLostMonth;
	
	/**
	 * Profit/lost for a last year
	 */
	@FXML
	private Label lbProfitLostJahr;
	
	/**
	 * Profit/lost for a last 3 year
	 */
	@FXML
	private Label lbProfitLost3Jahr;
	
	/**
	 * Profit/lost for a last 5 year
	 */
	@FXML
	private Label lbProfitLost5Jahr;
	
	/**
	 * Profit/lost over all time
	 */
	@FXML
	private Label lbProfitLostTotal;
	
	/**
	 * Total P/L
	 */
	@FXML
	private Label lbTotalPL;
	
	/**
	 * Event handler for language selection menu items
	 */
	private EventHandler<ActionEvent> langMenuEventHandler = new EventHandler<ActionEvent>() {
		public void handle(final ActionEvent event) {
			mnuLangEn.setSelected(false);
			mnuLangDe.setSelected(false);
			
			if (event.getSource() == mnuLangEn) {
	        	Main.getPrefs().put(Constants.PREFS_LOCALE, Constants.LOCALE_EN);
	        	mnuLangEn.setSelected(true);
	        } else if (event.getSource() == mnuLangDe) {
	        	Main.getPrefs().put(Constants.PREFS_LOCALE, Constants.LOCALE_DE);
	        	mnuLangDe.setSelected(true);
	        }
	        
	        Main.showAlert(AlertType.INFORMATION, "Language", "The application language has been changed", "Changes will take effect the next time you start the application");
		}
	};

	/**
	 * Initialize form
	 */
	public void initialize(final URL location, final ResourceBundle resources) {
		try {
			Main.addListener(this);

			initMenuItems();
			
			initBuySellDialog();
			initEditStocksDialog();
			initSettingsDialog();
			initImportDialog();
			initAboutDialog();
			initStockHistoryDialog();
			
			DBFacade.openSession();

			initControls();
			loadProviders();
			initStockTable();
			initHistoryChart();
			startUpdateTask();

		} catch (Exception e) {
			Main.showAlert(AlertType.ERROR, "Error", "Error form initialization", e.getMessage());
		}
	}
	
	/**
	 * App closing: close database
	 */
	public void onAppClose() {
		try {
			TimeoutController.execute(new Runnable() {
				public void run() {
					try {
						stopUpdateTask();
						DBFacade.closeSession();
					} catch (DatabaseException e) {
					}
				}
			}, 5 * 1000);
		} catch (TimeoutException e) {
		}
	}
	
	/**
	 * Load list of data providers
	 */
	private void loadProviders() {
		final Preferences prefs = Main.getPrefs();
		
		cbProvider.getItems().clear();
		
		for (final IProvider provider: FinanceService.getInst().getProviders()) {
			cbProvider.getItems().add(provider.getName());
		}
		cbProvider.getSelectionModel().select(prefs.getInt(PREFS_PROVIDER_IDX, 0));
		
		cbProvider.valueProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue.contentEquals(AlphaVantageProvider.PROVIDER_NAME) && settingsController.getVantageAPI().isEmpty()) {
					Main.showAlert(AlertType.INFORMATION, "No API key", "No API key for " + AlphaVantageProvider.PROVIDER_NAME, "Specify in settings your API key for provider");
					cbProvider.getSelectionModel().select(0);
				}
				
				if (newValue.contentEquals(WorldTradingDataProvider.PROVIDER_NAME) && settingsController.getWorldTradingAPI().isEmpty()) {
					Main.showAlert(AlertType.INFORMATION, "No API key", "No API key for " + WorldTradingDataProvider.PROVIDER_NAME, "Specify in settings your API key for provider");
					cbProvider.getSelectionModel().select(0);
				}
				
				prefs.putInt(PREFS_PROVIDER_IDX, cbProvider.getSelectionModel().getSelectedIndex());
			}
		});
	}

	/**
	 * Start timer to periodical update stock price
	 */
	private void startUpdateTask() {
		updateTimer = new Timer();

		updateTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				FinanceService.getInst().setProvider(FinanceService.getInst().getProvider(cbProvider.getSelectionModel().getSelectedIndex()));
				
				synchronized (stocksMutex) {
					for (final Stock stock: stocks) {
						try {
							FinanceService.getInst().updatePrice(stock);
						} catch (ProviderException e) {
						}
					}
					
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							updateTotalPrice();							
						}
					});
					
				}
				
				Platform.runLater(new Runnable() {
					public void run() {
						tblStocks.refresh();
					}
				});
			}}, 0, UPDATE_INTERVAL);
	}
	
	/**
	 * Stop update timer
	 */
	private void stopUpdateTask() {
		if (updateTimer != null) {
			updateTimer.cancel();
		}
	}
	
	/**
	 * Initialize ProgressView.fxml view
	 * 
	 * @throws IOException 
	 */
	private void initBuySellDialog() throws IOException {
		if (buySellStage == null) {
			final FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/BuySellView.fxml"));
			loader.setResources(Main.getResourceBundle());
		
			buySellStage = new Stage();
			
			final AnchorPane pane = (AnchorPane)loader.load();
			buySellController = loader.getController();
			
			buySellStage.setScene(new Scene(pane));
			buySellStage.setTitle("Buy/sell stock");
			//buySellStage.setResizable(false);
			//buySellStage.setAlwaysOnTop(true);
			
			buySellController.init(buySellStage);
		}
	}
	
	/**
	 * Initialize ProgressView.fxml view
	 * 
	 * @throws IOException 
	 */
	private void initStockHistoryDialog() throws IOException {
		if (stockHistoryStage == null) {
			final FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/StockHistoryView.fxml"));
			loader.setResources(Main.getResourceBundle());
		
			stockHistoryStage = new Stage();
			
			final BorderPane pane = (BorderPane)loader.load();
			stockHistoryController = loader.getController();
			
			stockHistoryStage.setScene(new Scene(pane));
			stockHistoryStage.setTitle("Buy/sell history");
			stockHistoryStage.setResizable(false);
			stockHistoryStage.setAlwaysOnTop(true);
			
			stockHistoryController.init(stockHistoryStage);
		}
	}
	
	
	/**
	 * Initialize ProgressView.fxml view
	 * 
	 * @throws IOException 
	 */
	private void initEditStocksDialog() throws IOException {
		if (editStocksStage == null) {
			final FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/EditStocksView.fxml"));
			loader.setResources(Main.getResourceBundle());
		
			editStocksStage = new Stage();
			
			final AnchorPane pane = (AnchorPane)loader.load();
			editStocksController = loader.getController();
			
			editStocksStage.setScene(new Scene(pane));
			editStocksStage.setTitle("Edit stocks");
			//editStocksStage.setAlwaysOnTop(true);
			
			editStocksController.init(editStocksStage);
		}
	}
	
	/**
	 * Initialize AboutView.fxml view
	 * 
	 * @throws IOException 
	 */
	private void initSettingsDialog() throws IOException {
		if (settingsStage == null) {
			final FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/SettingsView.fxml"));
			loader.setResources(Main.getResourceBundle());
		
			settingsStage = new Stage();
			
			final AnchorPane pane = (AnchorPane)loader.load();
			settingsController = loader.getController();
			
			settingsStage.setScene(new Scene(pane));
			settingsStage.setTitle("Settings");
			settingsStage.setResizable(false);

			settingsController.init(settingsStage);
		}
	}
	
	/**
	 * Initialize AboutView.fxml view
	 * 
	 * @throws IOException 
	 */
	private void initImportDialog() throws IOException {
		if (importStage == null) {
			final FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/CSVImportView.fxml"));
			loader.setResources(Main.getResourceBundle());
		
			importStage = new Stage();
			
			final BorderPane pane = (BorderPane)loader.load();
			importController = loader.getController();
			
			importStage.setScene(new Scene(pane));
			importStage.setTitle("Settings");

			importController.init(importStage);
		}
	}
	
	/**
	 * Initialize AboutView.fxml view
	 * 
	 * @throws IOException 
	 */
	private void initAboutDialog() throws IOException {
		if (aboutStage == null) {
			final FXMLLoader loader = new FXMLLoader(Main.class.getResource("views/AboutView.fxml"));
			loader.setResources(Main.getResourceBundle());
		
			aboutStage = new Stage();
			
			final AnchorPane pane = (AnchorPane)loader.load();
			aboutController = loader.getController();
			
			aboutStage.setScene(new Scene(pane));
			aboutStage.setTitle("About...");
			aboutStage.setResizable(false);
			aboutStage.setAlwaysOnTop(true);
			
			aboutController.init(aboutStage);
		}
	}

	/**
	 * Initialize menu items
	 */
	private void initMenuItems() {
		mnuLangEn.setOnAction(langMenuEventHandler);
		mnuLangDe.setOnAction(langMenuEventHandler);
	}
	
	/**
	 * Price axis
	 */
	private NumberAxis priceAxis;
	
	/**
	 * Initialize history chart
	 */
	private void initHistoryChart() {
		chartHistory.setLegendVisible(true);

		historyData = FXCollections.observableArrayList();
		historySeries = new Series<String, Number>(historyData);
		chartHistory.getData().add(historySeries);
		
		priceAxis = (NumberAxis) chartHistory.getYAxis();
		priceAxis.setAutoRanging(false);
		priceAxis.setLabel("Price, EUR");
	}
	
	/**
	 * Initialize other controls
	 */
	private void initControls() {
		cbTimePeriod.getItems().clear();
		cbTimePeriod.getItems().addAll("1 month", "3 month", "6 month", "1 jahr", "3 jahr", "5 jahr", "10 jahr");
		cbTimePeriod.getSelectionModel().select(Main.getPrefs().getInt("history_period", 0));
		cbTimePeriod.valueProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> ov, String t, String t1) {
				Main.getPrefs().putInt("history_period", cbTimePeriod.getSelectionModel().getSelectedIndex());
				if (tblStocks.getSelectionModel().getSelectedItem() != null) {
					updateHistory();
					
				}
			}    
		});
	}
	
	/**
	 * Initialize import data table
	 * @throws Exception 
	 * @throws ClassNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	private void initStockTable() throws ClassNotFoundException, Exception {
		stocks = FXCollections.observableArrayList();
		stocks.addAll(DBFacade.getAllStocks());
		
		TableColumn<Stock, String> colName = new TableColumn<Stock, String>("Aktien");
		colName.setCellValueFactory(new PropertyValueFactory<Stock, String>("name"));
		colName.setPrefWidth(220);
		
		TableColumn<Stock, String> colSymbol = new TableColumn<Stock, String>("Zeichen");
		colSymbol.setCellValueFactory(new PropertyValueFactory<Stock, String>("symbol"));
		colSymbol.setPrefWidth(80);
		
		TableColumn<Stock, Integer> colAmount = new TableColumn<Stock, Integer>("Anzahl");
		colAmount.setCellValueFactory(new PropertyValueFactory<Stock, Integer>("existedCount"));
		colAmount.setPrefWidth(80);
		
		TableColumn<Stock, String> colTotalPrice = new TableColumn<Stock, String>("Gesamt Price");
		colTotalPrice.setCellValueFactory(new PropertyValueFactory<Stock, String>("totalPriceStr"));
		colAmount.setPrefWidth(80);
		
		TableColumn<Stock, Double> colProfit = new TableColumn<Stock, Double>("G/V");
		colProfit.setCellValueFactory(new PropertyValueFactory<Stock, Double>("profitStr"));
		colProfit.setPrefWidth(100);
		
		TableColumn<Stock, Double> colPrice = new TableColumn<Stock, Double>("Aktuelle Price");
		colPrice.setCellValueFactory(new PropertyValueFactory<Stock, Double>("actualPriceStr"));
		colPrice.setPrefWidth(100);

		tblStocks.getColumns().clear();
		tblStocks.getColumns().addAll(colName, colSymbol, colAmount, colPrice, colTotalPrice, colProfit);
		tblStocks.setItems(stocks);
		
		tblStocks.setRowFactory(new Callback<TableView<Stock>, TableRow<Stock>>() {
			@Override
			public TableRow<Stock> call(TableView<Stock> arg0) {
				return new TableRow<Stock>() {
		            @Override
		            protected void updateItem(Stock item, boolean empty) {
		                super.updateItem(item, empty);
		
		                if (item != null) {
		                	if (item.getProfit() > 0) {
		                		setStyle("-fx-background-color: #99ff99;");
		                	} else if (item.getProfit() < 0) {
		                		setStyle("-fx-background-color: #ffcccc;");
		                	} else {
		                		setStyle("");
		                	}
		                }
		            }
		        };
			}
		});

		tblStocks.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Stock>() {
			public void changed(ObservableValue<? extends Stock> obs, Stock oldSelection, Stock newSelection) {
				btnSell.setDisable(newSelection == null);
				
				if (newSelection != null) {
					lbStockSymbol.setText(newSelection.getSymbol());
					lbStockName.setText(newSelection.getName());
					
					if (newSelection.getActualPrice() != null) {
						lbStockPrice.setText(String.format("%.2f %s", newSelection.getActualPrice(), newSelection.getCurrency()));
					} else {
						lbStockPrice.setText(String.format("0,00 %s", newSelection.getCurrency()));
					}
					
					lbAvgSellPrice.setText(newSelection.getAvgSellPrice());
					
					updateHistory();
					updateStatistic();
				} else {
					lbStockSymbol.setText("-");
					lbStockName.setText("-");
					lbStockPrice.setText("-");
					lbAvgSellPrice.setText("-");
				}
			}
		});
		
		tblStocks.setOnMouseClicked(new EventHandler<MouseEvent>() {
			 public void handle(MouseEvent click) {
				 if (click.getClickCount() == 2) {
					 if (stockHistoryStage.isShowing()) {
						 stockHistoryStage.close();
					 }

					 stockHistoryController.setStock(tblStocks.getSelectionModel().getSelectedItem());
					 stockHistoryStage.showAndWait();
				 }
			 }
		});
	}
	
	/**
	 * Returns the Profit/Lost statistic since "days"
	 * 
	 * @param days	The days to calculate statistic
	 * @return
	 */
	private void updatePLStatistic(final Label label, final Stock stock, final int days) {
		final String currency = stock.getCurrency();
		double pl = stock.getPLStatistic(days);
		
		label.setText(String.format("%.2f %s", pl, currency));
		
		if (pl > 0) {
			label.setTextFill(Paint.valueOf("green"));
		} else if (pl < 0) {
			label.setTextFill(Paint.valueOf("red"));
		} else {
			label.setTextFill(Paint.valueOf("black"));
		}
	}
	
	/**
	 * Update stock statistik
	 */
	private void updateStatistic() {
		final Stock stock = tblStocks.getSelectionModel().getSelectedItem();
		
		if (stock != null) {
			updatePLStatistic(lbProfitLostDay, 		stock, 1);
			updatePLStatistic(lbProfitLostWeek,		stock, 7);
			updatePLStatistic(lbProfitLostMonth,	stock, 30);
			updatePLStatistic(lbProfitLostJahr, 	stock, 365);
			updatePLStatistic(lbProfitLost3Jahr, 	stock, 365 * 3);
			updatePLStatistic(lbProfitLost5Jahr, 	stock, 365 * 5);
			updatePLStatistic(lbProfitLostTotal, 	stock, 0);
		}
	}
	
	/**
	 * Update total P/L (Profit/Lost) price 
	 */
	private void updateTotalPrice() {
		double totalPrice = 0;
		
		for (Stock stock: tblStocks.getItems()) {
			totalPrice += stock.getPLStatistic(0);
		}
		
		lbTotalPL.setText(String.format("%.2f EUR", totalPrice));
	}
	
	/**
	 * Update history graph by selecting a stock
	 */
	private void updateHistory() {
		try {
			FinanceService.getInst().setProvider(FinanceService.getInst().getProvider(cbProvider.getSelectionModel().getSelectedIndex()));
			
			final Stock stock = tblStocks.getSelectionModel().getSelectedItem();
			
			EHistoryPeriod period = EHistoryPeriod.HISTORY_1_JAHR;
			switch (cbTimePeriod.getSelectionModel().getSelectedIndex()) {
			case 0:
				period = EHistoryPeriod.HISTORY_1_MONTH;
				break;
			case 1:
				period = EHistoryPeriod.HISTORY_3_MONTH;
				break;
			case 2:
				period = EHistoryPeriod.HISTORY_6_MONTH;
				break;
			case 3:
				period = EHistoryPeriod.HISTORY_1_JAHR;
				break;
			case 4:
				period = EHistoryPeriod.HISTORY_3_JAHR;
				break;
			case 5:
				period = EHistoryPeriod.HISTORY_5_JAHR;
				break;
			case 6:
				period = EHistoryPeriod.HISTORY_10_JAHR;
				break;
			}
			
			FinanceService.getInst().updateHistory(stock, period);
			historyData.clear();
			
			if (stock.getHistory() != null) {
				final HistoryEntry [] hist = stock.getHistory();
				double min = hist[0].getPrice();
				double max = min;
				
				////////////////////////////
				historySeries.setName("");
				chartHistory.getData().clear();
				chartHistory.getData().add(historySeries);

				for (final StockBuy order: tblStocks.getSelectionModel().getSelectedItem().getBuyOrders()) {
					if (order.getPrice() > max) {
						max = order.getPrice();
					}
					if (order.getPrice() < min) {
						min = order.getPrice();
					}
					
					final ObservableList<Data<String, Number>> buyPrice = FXCollections.observableArrayList();
					buyPrice.add(new Data<String, Number>(chartDateFormat.format(hist[0].getDate().getTime()), order.getPrice()));
					buyPrice.add(new Data<String, Number>(chartDateFormat.format(hist[hist.length - 1].getDate().getTime()), order.getPrice()));
					
					final Series<String, Number> newSerie = new Series<String, Number>(buyPrice);
					newSerie.setName(order.getDateStr());
					chartHistory.getData().add(newSerie);
				}
				////////////////////////////
				
				for (int i = 0; i < hist.length; i++) {
					if (hist[i].getPrice() > max) {
						max = hist[i].getPrice();
					}
					if (hist[i].getPrice() < min) {
						min = hist[i].getPrice();
					}
					historyData.add(new Data<String, Number>(chartDateFormat.format(hist[i].getDate().getTime()), hist[i].getPrice()));
				}
				
				double toleranz = (max - min) * 0.1;
				priceAxis.setLowerBound(min - toleranz);
				priceAxis.setUpperBound(max + toleranz);
			}
		} catch (ProviderException e) {
		}
	}
	
	/**
	 * Open about window
	 */
	@FXML
	public void onAboutClicked() {
		aboutStage.showAndWait();
	}
	
	/**
	 * Show sell window
	 * @throws DatabaseException 
	 */
	@FXML 
	public void onSellClicked() throws DatabaseException {
		buySellStage.setTitle("Aktienverkauf");

		buySellController.setStockList(DBFacade.getAllStocks(), tblStocks.getSelectionModel().getSelectedIndex());
		buySellStage.showAndWait();

		tblStocks.refresh();
	}
	
	/**
	 * Show buy window
	 * @throws DatabaseException 
	 */
	@FXML
	public void onBuyClicked() throws DatabaseException {
		buySellStage.setTitle("Aktienkauf");
		
		buySellController.setStockList(DBFacade.getAllStocks(), tblStocks.getSelectionModel().getSelectedIndex());
		buySellStage.showAndWait();

		tblStocks.refresh();
	}

	/**
	 * Edit stocks list
	 */
	@FXML
	public void onEditStocksClicked() {
		editStocksController.updateStocks();
		editStocksStage.showAndWait();
		try {
			synchronized (stocksMutex) {
				stocks.clear();
				stocks.addAll(DBFacade.getAllStocks());	
			}

			tblStocks.refresh();
		} catch (Exception e) {
			Main.showAlert(AlertType.ERROR, "Error", "Update data error", e.getMessage());
		}
	}
	
	/**
	 * Open program settings
	 */
	@FXML
	public void onSettingsMenuClicked() {
		settingsStage.showAndWait();
	}
	
	/**
	 * Import CSV/Excel clicked
	 */
	@FXML
	public void onImportClicked() {
		importController.clearForm();
		importStage.showAndWait();

		try {
			synchronized (stocksMutex) {
				stocks.clear();
				stocks.addAll(DBFacade.getAllStocks());	
			}
			tblStocks.refresh();
		} catch (DatabaseException e) {
		}
	}
	
	/**
	 * Export data to CSV format
	 */
	@FXML
	public void onExportClicked() {
		final FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("MS Excel (*.xls)", "*.xls"),
												 new FileChooser.ExtensionFilter("CSV file (*.csv)", "*.csv"));

		final String initialDir = Main.getPrefs().get("export_path", "");
		if (!initialDir.isEmpty()) {
			fileChooser.setInitialDirectory(new File(initialDir));
		}
		
        final File file = fileChooser.showSaveDialog(Main.getPrimaryStage());

        if (file != null) {
        	Main.getPrefs().put("export_path", file.getParent());
        	
        	if (file.getName().contains(".xls")) {
        		exportExcel(file);
        	} else {
        		exportCSV(file);
        	}
        }
	}
	
	/**
	 * Export database to CSV file
	 * 
	 * @param file
	 */
	private void exportCSV(final File file) {
		BufferedWriter writer = null;
    	try {
    		writer = new BufferedWriter(new FileWriter(file));
    		writer.write("Bezeichnung;ISIN/Symbol;Stk./Nominale;Akt. Kurs;;Gesamt Price;;Gewinn/Verlust;\n");
    		
    		for (Stock stock: tblStocks.getItems()) {
    			writer.write(String.format("%s;%s;%d;%.2f;%s;%.2f;%s;%.2f;%s\n", stock.getName(), stock.getSymbol(), stock.getExistedCount(),
    							stock.getActualPrice(), stock.getCurrency(), stock.getExistedCount() * stock.getActualPrice(),
    							stock.getCurrency(), stock.getPLStatistic(0), stock.getCurrency()));

    			TableColumn<Stock, Double> colProfit = new TableColumn<Stock, Double>("G/V");
    			colProfit.setCellValueFactory(new PropertyValueFactory<Stock, Double>("profitStr"));
            }
    		
    		Main.showAlert(AlertType.INFORMATION, "CSV Export", "Export finished", "Done");
    	} catch (Exception e) {
    		Main.showAlert(AlertType.ERROR, "Error", "Error by saving CSV", e.getMessage());
    	} finally {
    		try {
    			writer.close();
    		} catch (Exception e) {}
    	}
	}
	
	/**
	 * Export database to MS Excel
	 * 
	 * @param file
	 */
	private void exportExcel(final File file) {
		ExcelWriter writer = null;
		
		try {
			writer = new ExcelWriter(file);
			writer.createSheet("Exported stocks");
			
			writer.writeString(0, 0, "Bezeichnung");
			writer.writeString(1, 0, "ISIN/Symbol");
			writer.writeString(2, 0, "Stk./Nominale");
			writer.writeString(3, 0, "Akt. Kurs");
			writer.writeString(5, 0, "Gesamt price");
			writer.writeString(7, 0, "Gewinn/Verlust");
			
			int row = 1;
			for (Stock stock: tblStocks.getItems()) {
				writer.writeString(0, row, stock.getName());
				writer.writeString(1, row, stock.getSymbol());
				writer.writeInt(2, row, stock.getExistedCount());
				writer.writeDouble(3, row, stock.getActualPrice());
				writer.writeString(4, row, stock.getCurrency());
				writer.writeDouble(5, row, stock.getExistedCount() * stock.getActualPrice());
				writer.writeString(6, row, stock.getCurrency());
				writer.writeDouble(7, row, stock.getPLStatistic(0));
				writer.writeString(8, row, stock.getCurrency());
				
				row++;
            }
			
			Main.showAlert(AlertType.INFORMATION, "MS Excel Export", "Export finished", "Done");
			
		} catch (Exception e) {
			Main.showAlert(AlertType.ERROR, "Error", "Excel export error", e.getMessage());
		} finally {
			if (writer != null) {
				try {
					writer.closeDocument();
				} catch (ExcelDocumentException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
