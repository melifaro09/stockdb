package de.malex.stockdb.controllers.readers;

import java.io.File;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;

/**
 * Class to write Excel Documents
 * 
 * @author Alexandr Mitiaev
 */
public class ExcelWriter {

	/**
	 * The {@link Workbook} to use
	 */
	private WritableWorkbook workbook = null;
	
	/**
	 * Active {@link Sheet} from workbook
	 */
	private WritableSheet activeSheet;
	
	/**
	 * Font for the Document
	 */
	private WritableCellFormat times;
	
	/**
	 * Name of the file
	 */
	private String fileName;
	
	/**
 	 * Open Excel-Document by a given name
 	 * 
 	 * @exception IOException if can't open file
	 */
	public ExcelWriter(String fileName) throws IOException {
		createFont();
		prepareFile(new File(fileName));
	}

	/**
 	 * Open Excel-Document by a given name
 	 * 
 	 * @exception IOException if can't open file
	 */
	public ExcelWriter(File file) throws IOException {
		createFont();
		prepareFile(file);
	}

	/**
	 * Create font for document
	 */
	private void createFont() {
	    try {
	    	times = new WritableCellFormat(new WritableFont(WritableFont.TAHOMA, 10));
			times.setWrap(false);
		} catch (WriteException e) {
			// Nothing
		}
	}
	
	/**
	 * Close Excel Document
	 * 
	 * @throws ExcelDocumentException 
	 */
	public void closeDocument() throws ExcelDocumentException {
		if (workbook != null) {
			try {
				workbook.write();
				workbook.close();
			} catch (WriteException | IOException e) {
				throw new ExcelDocumentException("Error closing Excel Document");
			}
			
			workbook = null;
		}
	}
	
	/**
	 * Open Excel Document, extract sheets etc.
	 * 
	 * @param file Excel {@link File} to open
	 */
	private void prepareFile(File file) throws IOException {
		if (file != null) {
			fileName = file.getAbsolutePath();
			workbook = Workbook.createWorkbook(file);
		} else {
			throw new IOException("Can't open Excel file (File is null)");
		}
	}
	
	/**
	 * Create new sheet
	 * 
	 * @param name Name of the new sheet
	 */
	public void createSheet(String name) {
		activeSheet = workbook.createSheet(name, 0);
	}

	
	/**
	 * Write double value to the active sheet
	 * 
	 * @param col The column number
	 * @param row The row number
	 * @param value The value to write
	 * 
	 * @throws ExcelDocumentException 
	 */
	public void writeDouble(int col, int row, double value) throws ExcelDocumentException {
	    try {
	    	Number number = new Number(col, row, value, times);
			activeSheet.addCell(number);
		} catch (WriteException e) {
			throw new ExcelDocumentException("Error writing double value");
		}
	}
	
	/**
	 * Write integer value to the active sheet
	 * 
	 * @param col The column number
	 * @param row The row number
	 * @param value The value to write
	 * 
	 * @throws ExcelDocumentException 
	 */
	public void writeInt(int col, int row, int value) throws ExcelDocumentException {
		try {
	    	Number number = new Number(col, row, value, times);
			activeSheet.addCell(number);
		} catch (WriteException e) {
			throw new ExcelDocumentException("Error writing integer value");
		}
	}
	
	/**
	 * Write string to the active sheet
	 * 
	 * @param col The column number
	 * @param row The row number
	 * @param value The string to write
	 * 
	 * @throws ExcelDocumentException 
	 */
	public void writeString(int col, int row, String value) throws ExcelDocumentException {
	    try {
	    	Label label = new Label(col, row, value, times);
			activeSheet.addCell(label);
		} catch (WriteException e) {
			throw new ExcelDocumentException("Error writing string");
		}
	}
	
	/**
	 * Write a matrix in the active sheet
	 * 
	 * @param startRow Start row index
	 * @param startCol Start column index
	 * @param value The matrix to write
	 * 
	 * @return Readed table as a double array
	 * 
	 * @throws ExcelDocumentException 
	 */
	public void writeMatrix(int startCol, int startRow, double [][] value) throws ExcelDocumentException {
		
		for (int i = startRow; i < startRow + value.length; i++) {
			for (int j = startCol; j < startCol + value[0].length; j++) {
				writeDouble(j, i, value[i - startRow][j - startCol]);
			}
		}
	}
	
	/**
	 * Return Excel Document file name
	 */
	public String getSourceFileName() {
		return fileName;
	}
	
	/**
	 * Exceptions of the {@link ExcelReader} class
	 * 
	 * @author Alexandr Mitiaev
	 */
	public class ExcelDocumentException extends Exception {
		/**
		 * Generated Serial Version UID
		 */
		private static final long serialVersionUID = 1915444066749155767L;

		/**
		 * Create new {@link ExcelDocumentException} object
		 * 
		 * @param message Exception message
		 */
		public ExcelDocumentException(String message) {
			super(message);
		}
	}
}
