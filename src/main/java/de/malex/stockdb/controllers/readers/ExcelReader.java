package de.malex.stockdb.controllers.readers;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.CellType;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * Class to represent one Excel Document
 * 
 * @author Alexandr Mitiaev
 */
public class ExcelReader {
	
	/**
	 * Error message string "no active sheet"
	 */
	private static final String ERROR_NO_ACTIVE_SHEET		=		"No active sheet selected";
	
	/**
	 * Error message string "invalid cell type"
	 */
	private static final String ERROR_TYPE_INVALID			=		"Invalid type of the cell";
	
	/**
	 * The {@link Workbook} to use
	 */
	private Workbook workbook = null;
	
	/**
	 * The list of available {@link Sheet}s
	 */
	private Sheet [] sheets = null;
	
	/**
	 * Active {@link Sheet} from workbook
	 */
	private Sheet activeSheet;
	
	/**
	 * Name of the file
	 */
	private String fileName;
	
	/**
	 * List of the charachters to remove from content
	 */
	private String charFilter = "";
	
	/**
 	 * Open Excel-Document by a given name
 	 * 
 	 * @exception IOException if can't open file
	 */
	public ExcelReader(String fileName) throws IOException {
		prepareFile(new File(fileName));
	}

	/**
 	 * Open Excel-Document by a given name
 	 * 
 	 * @exception IOException if can't open file
	 */
	public ExcelReader(File file) throws IOException {
		prepareFile(file);
	}
	
	/**
	 * Close Excel Document
	 */
	public void closeDocument() {
		if (workbook != null) {
			workbook.close();
			workbook = null;
		}
	}
	
	/**
	 * Set list of the symbols to delete from content
	 * before convert it to int/double values
	 * 
	 * Example of the filter: "ABC"
	 * 
	 * @param filter Filter to set
	 */
	public void setCharFilter(String filter) {
		charFilter = "[" + filter + "]";
	}
	
	/**
	 * Open Excel Document, extract sheets etc.
	 * 
	 * @param file Excel {@link File} to open
	 */
	private void prepareFile(File file) throws IOException {
		if (file != null) {
			
			fileName = file.getAbsolutePath();
			
			try {
				workbook = Workbook.getWorkbook(file);
				sheets = workbook.getSheets();
			} catch (BiffException e) {
				throw new IOException("Error by opening Excel Document");
			}
		} else {
			throw new IOException("Can't open Excel file (File is null)");
		}
	}
	
	/**
	 * Returns the cell with a given indexes
	 * 
	 * @param col Number of the column
	 * @param row Number of the row
	 * 
	 * @return {@link Cell}
	 * @throws ExcelDocumentException 
	 */
	private Cell getCell(int col, int row) throws ExcelDocumentException {
		if (activeSheet == null)
			throw new ExcelDocumentException(ERROR_NO_ACTIVE_SHEET);

		return activeSheet.getCell(col, row);
	}
	
	/**
	 * Returns the content of the cell with considering
	 * of a filter
	 * 
	 * @param col Column index
	 * @param row Row index
	 * 
	 * @return Cell content as a string
	 * @throws ExcelDocumentException 
	 */
	private String getCellContent(int col, int row) throws ExcelDocumentException {
		Cell cell = getCell(col, row);
		
		String result = cell.getContents();
		
		if (charFilter != "") {
			result = result.replaceAll(charFilter, "");
		}
		
		return result;
	}
	
	/**
	 * Get number of the sheets in file
	 * 
	 * @return
	 */
	public int getSheetCount() {
		if (sheets != null)
			return sheets.length;
		else 
			return -1;
	}
	
	/**
	 * Set the sheet with a given index as current for all operations
	 * 
	 * @param index Index of the sheet
	 */
	public void setActiveSheet(int index) {
		if (sheets != null && index < sheets.length) {
			activeSheet = sheets[index];
		} else {
			activeSheet = null;
		}
	}
	
	/**
	 * Return true, if cell is empty, otherwise false
	 * 
	 * @param col The column number
	 * @param row The row number
	 * 
	 * @return true, if cell is empty, otherwise false
	 * @throws ExcelDocumentException 
	 */
	public boolean isCellEmpty(int col, int row) throws ExcelDocumentException {
		return getCellContent(col, row).isEmpty();
	}
	
	/**
	 * Read double value from sheets cell
	 * 
	 * @param col The column number
	 * @param row The row number
	 * 
	 * @return Double value
	 * @throws ExcelDocumentException 
	 */
	public double readDouble(int col, int row) throws ExcelDocumentException {
		Cell cell = getCell(col, row);
		
		if (cell.getType() != CellType.NUMBER && cell.getType() != CellType.NUMBER_FORMULA) {
			throw new ExcelDocumentException(ERROR_TYPE_INVALID);
		} else {
			return ((NumberCell)cell).getValue();
		}
	}
	
	/**
	 * Read integer value from sheets cell
	 * 
	 * @param col The column number
	 * @param row The row number
	 * 
	 * @return Integer value
	 * @throws ExcelDocumentException 
	 */
	public int readInt(int col, int row) throws ExcelDocumentException {
		Cell cell = getCell(col, row);
		
		if (cell.getType() != CellType.NUMBER && cell.getType() != CellType.NUMBER_FORMULA) {
			throw new ExcelDocumentException(ERROR_TYPE_INVALID);
		}
		
		return Integer.parseInt(getCellContent(col, row));
	}
	
	/**
	 * Read string value from sheets cell
	 * 
	 * @param mMainSheet The sheet
	 * @param col The column number
	 * @param row The row number
	 * 
	 * @return String value
	 * @throws ExcelDocumentException 
	 */
	public String readString(int col, int row) throws ExcelDocumentException {
		return getCellContent(col, row);
	}
	
	/**
	 * Read a table as an array with double values
	 * 
	 * @param startRow Start row index
	 * @param startCol Start column index
	 * @param endRow End row index
	 * @param endCol End column index
	 * 
	 * @return Readed table as a double array
	 * 
	 * @throws ExcelDocumentException 
	 */
	public double [][] readDoubleTable(int startCol, int startRow, int endCol, int endRow) throws ExcelDocumentException {
		
		double [][] result = new double[endRow - startRow + 1][endCol - startCol + 1];
		
		for (int i = startRow; i <= endRow; i++) {
			for (int j = startCol; j <= endCol; j++) {
				result[i][j] = readDouble(j, i);
			}
		}
		
		return result;
	}
	
	/**
	 * Read table at given started cell and to end
	 * 
	 * @param startRow Start row of the table
	 * @param startCol Start column of the table
	 * 
	 * @return Readed table as a double array
	 * @throws ExcelDocumentException 
	 */
	public double [][] readDoubleTable(int startCol, int startRow) throws ExcelDocumentException {
		
		Cell cell;
		int endCol = startCol;
		int endRow = startRow;
		
		// Search end column
		do {
			try {
				cell = getCell(++endCol, startRow);
			} catch (Exception e) {
				break;
			}
		} while (cell.getType() == CellType.NUMBER || cell.getType() == CellType.NUMBER_FORMULA);
		endCol--;
		
		// Search end row
		do {
			try {
				cell = getCell(startCol, ++endRow);
			} catch (Exception e) {
				break;
			}
		} while (cell.getType() == CellType.NUMBER || cell.getType() == CellType.NUMBER_FORMULA);
		endRow--;
		
		return readDoubleTable(startCol, startRow, endCol, endRow);
	}
	
	/**
	 * Return Excel Document file name
	 */
	public String getSourceFileName() {
		return fileName;
	}
	
	/**
	 * Exceptions of the {@link ExcelReader} class
	 * 
	 * @author Alexandr Mitiaev
	 */
	public class ExcelDocumentException extends Exception {
		/**
		 * Generated Serial Version UID
		 */
		private static final long serialVersionUID = 1915444066749155767L;

		/**
		 * Create new {@link ExcelDocumentException} object
		 * 
		 * @param message Exception message
		 */
		public ExcelDocumentException(String message) {
			super(message);
		}
	}
}
