package de.malex.stockdb.controllers;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import de.malex.stockdb.db.model.Stock;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import de.malex.stockdb.Main;
import de.malex.stockdb.db.DBFacade;
import de.malex.stockdb.db.DatabaseException;
import de.malex.stockdb.db.model.StockSell;
import de.malex.stockdb.db.model.StockBuy;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * The controller for the ProgressView.fxml form
 * 
 * @author Alexandr Mitiaev
 */
public class BuySellViewController implements Initializable {
	/**
	 * Window stage
	 */
	private Stage stage;
	/**
	 * Actuall edited stock
	 */
	@FXML
	private ComboBox<Stock> cbStock;
	
	/**
	 * Stock number
	 */
	@FXML
	private TextField edNumber;
	
	/**
	 * Stock price
	 */
	@FXML
	private TextField edPrice;
	
	/**
	 * List of users
	 */
	private ObservableList<Stock> stocks;
	
	/**
	 * Currency symbol
	 */
	@FXML
	private Label lbCurrency;
	
	/**
	 * Initialize form
	 */
	public void initialize(URL location, ResourceBundle resources) {
		stocks = FXCollections.observableArrayList();
		cbStock.setItems(stocks);
		
		cbStock.valueProperty().addListener(new ChangeListener<Stock>() {
			public void changed(ObservableValue<? extends Stock> observable, Stock oldValue, Stock newValue) {
				if (newValue != null && newValue.getActualPrice() != null) {
					edPrice.setText(String.format("%.2f", newValue.getActualPrice()));
					lbCurrency.setText(newValue.getCurrency());
				} else {
					edPrice.setText("");
					lbCurrency.setText("");
				}
			}
		});
	}
	
	/**
	 * Set up views stage
	 * 
	 * @param stage The {@link Stage}
	 */
	public void init(Stage stage) {
		this.stage = stage;
	}
	
	/**
	 * Check values in input fields
	 */
	private boolean checkInput() {
		if (edNumber.getText().isEmpty() || edPrice.getText().isEmpty()) {
			Main.showAlert(AlertType.WARNING, "Sell/buy stock", "Error saving sell order", "Required fields are not filled");
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Save button clicked
	 * @throws DatabaseException 
	 */
	@FXML
	public void onCloseClicked() throws DatabaseException {	
		if (!checkInput()) {
			return;
		}
		
		final Stock stock = cbStock.getSelectionModel().getSelectedItem();
		
		if (stage.getTitle() == "Aktienverkauf") {
			final StockSell stockSell = new StockSell();
			stockSell.setAmount(Integer.valueOf(edNumber.getText()));
			stockSell.setPrice(Double.valueOf(edPrice.getText().replace(",", ".")));
			stockSell.setDate(new Date());
			stockSell.setStock(stock);
			DBFacade.saveStockSell(stockSell);
			stock.addSellOrder(stockSell);
		} else {
			final StockBuy stockBuy = new StockBuy();
			stockBuy.setAmount(Integer.valueOf(edNumber.getText()));
			stockBuy.setPrice(Double.valueOf(edPrice.getText().replace(",", ".")));
			stockBuy.setDate(new Date());
			stockBuy.setStock(stock);
			DBFacade.saveStockBuy(stockBuy);
			stock.addBuyOrder(stockBuy);
		}
		
		DBFacade.saveStock(stock);

		stage.close();
	}
	
	/**
	 * Setup stock list
	 * 
	 * @param stockList	The list of {@link Stock}s
	 * @param index			Selected stock index
	 */
	public void setStockList(final List<Stock> stockList, int index) {
		stocks.clear();
		stocks.addAll(stockList);
		
		cbStock.getSelectionModel().select(index);
		
		edNumber.setText("");
		edNumber.requestFocus();
	}
}
