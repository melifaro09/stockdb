package de.malex.stockdb.financial.providers;

import de.malex.stockdb.db.model.Stock;
import de.malex.stockdb.financial.providers.FinanceService.EHistoryPeriod;

/**
 * The financial data provider interface
 * 
 * @author Alexandr Mitiaev
 *
 */
public interface IProvider {
	/**
	 * User-readable name of the provider
	 * 
	 * @return Name of the provider
	 */
	public String getName();
	
	/**
	 * Search stock by symbol
	 * 
	 * @param symbol	The stock symbol
	 * 
	 * @return	The {@link Stock} instance
	 * 
	 * @throws ProviderException 
	 */
	public Stock search(final String symbol) throws ProviderException;
	
	/**
	 * Returns the historical data
	 * 
	 * @param symbol	The stock symbol
	 * 
	 * @throws ProviderException 
	 */
	public HistoryEntry [] getHistory(final String symbol, final EHistoryPeriod period) throws ProviderException;
	
	/**
	 * Returns exchange curse for currency relatively EUR
	 * 
	 * @param currency
	 * @return
	 */
	public Double getExchangeCurse(final String currency);
}
