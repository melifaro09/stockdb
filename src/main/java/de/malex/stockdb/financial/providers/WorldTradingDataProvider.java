package de.malex.stockdb.financial.providers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import de.malex.stockdb.db.model.Stock;
import de.malex.stockdb.financial.providers.FinanceService.EHistoryPeriod;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import yahoofinance.quotes.fx.FxQuote;

/**
 * Alpha Vantage provider
 * 
 * Documentation: http://www.alphavantage.co/documentation
 * API key: http://www.alphavantage.co/support/#api-key
 * 
 * @author Alexandr Mitiaev
 *
 */
public class WorldTradingDataProvider implements IProvider {
	/**
	 * Provider public name
	 */
	public final static String PROVIDER_NAME		=			"World Trading Data";
	
	/**
	 * API Key
	 */
	private static final String API_KEY				=			"hdgU9PGDK1Xrx2xp3h01YbJuVbZUW4cwnWUQ8PEdqPU84esnBwABHeD7N1PY";
	
	/**
	 * EUR/USD exchange price
	 */
	private JSONObject exchangeRate;
	
	/**
	 * API key for service
	 */
	private static String apiKey = "";
	
	/**
	 * {@inherit}
	 */
	public String getName() {
		return PROVIDER_NAME;
	}
	
	/**
	 * Set API key
	 * 
	 * @param apiKey
	 */
	public void setAPI(final String apiKey) {
		this.apiKey = apiKey;
	}
	
	/**
	 * Download EUR/USD currency exchange values if not existed
	 * 
	 * @throws IOException 
	 */
	private void checkExchange() throws IOException {
		if (exchangeRate == null) {
			final JSONObject response = readJsonFromUrl(String.format("https://api.worldtradingdata.com/api/v1/forex?base=%s&api_token=%s", "EUR", API_KEY));
			exchangeRate = (JSONObject)response.get("data");
		}
	}

	/**
	 * {@inherit}
	 */
	public Stock search(final String symbol) throws ProviderException {
		try {
			checkExchange();
			
			final JSONObject response = readJsonFromUrl(String.format("https://api.worldtradingdata.com/api/v1/forex?search_term=%s&search_by=symbol,name&api_token=%s", URLEncoder.encode(symbol, "UTF-8"), API_KEY));
			
			System.out.println(response);
			response.get("data");
			
			return null;
			/*
			final yahoofinance.Stock stock = YahooFinance.get(symbol.toUpperCase());
			
			if (stock != null && stock.getName() != null) {
				final Stock result = new Stock();
				
				result.setName(stock.getName());
				result.setSymbol(symbol.toUpperCase());
				try {
					result.setActualPrice(stock.getQuote().getPrice().doubleValue());
					result.setCurrency(stock.getCurrency());
					
					if (result.getCurrency().contentEquals("USD")) {
						result.setActualPrice(result.getActualPrice() / eurusd);
						result.setCurrency("EUR");
					}
				} catch (Exception e) {
				}
				
				return result;
			} else {
				return null;
			}
			*/
		} catch (IOException e) {
			return null;
		}
	}
	
	/**
	 * Read all bytes from a {@link Reader} instance
	 * 
	 * @param rd		The {@link Reader} instance
	 * 
	 * @return
	 * 
	 * @throws IOException
	 */
	private static String readAll(final Reader rd) throws IOException {
		final StringBuilder sb = new StringBuilder();
		int cp;
		
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}

		return sb.toString();
	}
	
	/**
	 * Reading JSON object from an URL
	 * 
	 * @param url		The URL to obtain JSON
	 * 
	 * @return			The {@link JSONObject} instance
	 * 
	 * @throws IOException
	 * @throws JSONException
	 */
	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
	    final InputStream is = new URL(url).openStream();
	    
	    try {
	    	final BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	    	final String jsonText = readAll(reader);
	    	JSONObject json = new JSONObject(jsonText);
	      
	    	return json;
	    } finally {
	    	is.close();
	    }
	}
	
	/**
	 * Returns the historical data
	 * 
	 * @param symbol	The stock symbol
	 * 
	 * @throws ProviderException 
	 */
	public HistoryEntry[] getHistory(final String symbol, final EHistoryPeriod period) throws ProviderException {
		/*
		try {
			JSONObject response = readJsonFromUrl("https://api.worldtradingdata.com/api/v1/stock?symbol=" + "eurusd" + "&api_token=" + API_KEY + "&output=json");
			System.out.println(response);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		try {
			final Calendar from = Calendar.getInstance();
			final Calendar to = Calendar.getInstance();
			
			switch (period) {
			case HISTORY_1_MONTH:
				from.add(Calendar.MONTH, -1);
				break;
			case HISTORY_3_MONTH:
				from.add(Calendar.MONTH, -3);
				break;
			case HISTORY_6_MONTH:
				from.add(Calendar.MONTH, -6);
				break;
			case HISTORY_1_JAHR:
				from.add(Calendar.YEAR, -1);
				break;
			}

			yahoofinance.Stock stock = YahooFinance.get(symbol.toUpperCase());
			final List<HistoricalQuote> quotes = stock.getHistory(from, to, Interval.DAILY);
			
			final HistoryEntry [] result = new HistoryEntry [quotes.size()];
			
			//for (int i = 0; i < result.length; i++) {
			//	result[i] = quotes.get(i).getClose().doubleValue();
			//	if (stock.getCurrency().contentEquals("USD")) {
			//		result[i] = result[i] / eurusd;
			//	}
			//}
			
			return result;
			
		} catch (Exception e) {
			return null;
		}
		*/
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getExchangeCurse(String currency) {
		return null;
	}
}
