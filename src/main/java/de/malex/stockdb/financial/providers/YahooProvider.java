package de.malex.stockdb.financial.providers;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import de.malex.stockdb.db.model.Stock;
import de.malex.stockdb.financial.providers.FinanceService.EHistoryPeriod;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import yahoofinance.quotes.fx.FxQuote;

/**
 * Yahoo finance provider
 * 
 * @author Alexandr Mitiaev
 *
 */
public class YahooProvider implements IProvider {
	/**
	 * Provider public name
	 */
	private final static String PROVIDER_NAME		=			"Yahoo Finance";

	/**
	 * Exchange curses for currencies
	 */
	private HashMap<String, Double> exchange;
	
	/**
	 * Create new instance of {@link YahooProvider}
	 */
	public YahooProvider() {
		exchange = new HashMap<String, Double>();
	}
	
	/**
	 * {@inherit}
	 */
	public String getName() {
		return PROVIDER_NAME;
	}

	/**
	 * {@inherit}
	 */
	public Stock search(final String symbol) throws ProviderException {
		try {
			final yahoofinance.Stock stock = YahooFinance.get(symbol.toUpperCase());
			
			if (stock != null && stock.getName() != null) {
				final Stock result = new Stock();
				
				result.setName(stock.getName());
				result.setSymbol(symbol.toUpperCase());
				try {
					result.setActualPrice(stock.getQuote().getPrice().doubleValue());
					result.setCurrency(stock.getCurrency());
					
					final String currency = result.getCurrency();
					
					if (!currency.contentEquals("EUR")) {
						result.setActualPrice(result.getActualPrice() / getExchangeCurse(currency));
						result.setCurrency("EUR");
					}
				} catch (Exception e) {
				}
				
				return result;
			} else {
				return null;
			}
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Returns the actual price
	 * 
	 * @param symbol	The stock symbol
	 * 
	 * @return			Actual price
	 */
	public double getPrice(final String symbol) {
		try {
			final yahoofinance.Stock stock = YahooFinance.get(symbol.toUpperCase());
			
			if (stock != null && stock.getName() != null) {
				return stock.getQuote().getPrice().doubleValue();
			} else {
				return 0;
			}
		} catch (IOException e) {
			return 0;
		}
	}
	
	/**
	 * Returns the historical data
	 * 
	 * @param symbol	The stock symbol
	 * 
	 * @throws ProviderException 
	 */
	public HistoryEntry [] getHistory(final String symbol, final EHistoryPeriod period) throws ProviderException {
		try {
			final Calendar from = Calendar.getInstance();
			final Calendar to = Calendar.getInstance();
			Interval interval = Interval.DAILY;
			
			switch (period) {
			case HISTORY_1_MONTH:
				from.add(Calendar.MONTH, -1);
				interval = Interval.DAILY;
				break;
			case HISTORY_3_MONTH:
				from.add(Calendar.MONTH, -3);
				interval = Interval.DAILY;
				break;
			case HISTORY_6_MONTH:
				from.add(Calendar.MONTH, -6);
				interval = Interval.DAILY;
				break;
			case HISTORY_1_JAHR:
				from.add(Calendar.YEAR, -1);
				interval = Interval.DAILY;
				break;
			case HISTORY_3_JAHR:
				from.add(Calendar.YEAR, -3);
				interval = Interval.WEEKLY;
				break;
			case HISTORY_5_JAHR:
				from.add(Calendar.YEAR, -5);
				interval = Interval.WEEKLY;
				break;
			case HISTORY_10_JAHR:
				from.add(Calendar.YEAR, -10);
				interval = Interval.WEEKLY;
				break;
			}

			yahoofinance.Stock stock = YahooFinance.get(symbol.toUpperCase());
			final List<HistoricalQuote> quotes = stock.getHistory(from, to, interval);
			
			final HistoryEntry [] result = new HistoryEntry [quotes.size()];
			
			for (int i = 0; i < result.length; i++) {
				result[i] = new HistoryEntry(quotes.get(i).getClose().doubleValue(), quotes.get(i).getDate());
				
				final String stockCurrency = stock.getCurrency();
				if (!stockCurrency.contentEquals("EUR")) {
					result[i].setPrice(result[i].getPrice() / getExchangeCurse(stockCurrency));
				}
			}
			
			return result;
			
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getExchangeCurse(String currency) {
		currency = currency.toUpperCase();
		
		if (!exchange.containsKey(currency)) {
			try {
				FxQuote quote = YahooFinance.getFx(String.format("EUR%s=X", currency));
				exchange.put(currency, quote.getPrice().doubleValue());
			} catch (IOException e) {
				return null;
			}
		}
		
		return exchange.get(currency);
	}
}
