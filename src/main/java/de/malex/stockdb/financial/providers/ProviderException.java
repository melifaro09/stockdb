package de.malex.stockdb.financial.providers;

/**
 * The exception class for financial data providers
 * 
 * @author Alexandr Mitiaev
 *
 */
public class ProviderException extends Exception {
	/**
	 * Serail version UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Create new {@link ProviderException} instance
	 */
	public ProviderException() {
		super();
	}
	
	/**
	 * Create new {@link ProviderException} based on another {@link Exception} instance 
	 * 
	 * @param ex	The base {@link Exception}
	 */
	public ProviderException(final Exception ex) {
		super(ex);
	}
	
	/**
	 * Create new {@link ProviderException} with a given message
	 * 
	 * @param message	The exception message
	 */
	public ProviderException(final String message) {
		super(message);
	}
}
