package de.malex.stockdb.financial.providers;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import de.malex.stockdb.db.model.Stock;
import de.malex.stockdb.financial.providers.FinanceService.EHistoryPeriod;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;
import yahoofinance.quotes.fx.FxQuote;

/**
 * Alpha Vantage provider
 * 
 * Documentation: http://www.alphavantage.co/documentation
 * API key: http://www.alphavantage.co/support/#api-key
 * 
 * @author Alexandr Mitiaev
 *
 */
public class AlphaVantageProvider implements IProvider {
	/**
	 * Provider public name
	 */
	public final static String PROVIDER_NAME		=			"Alpha Vantage";
	
	/**
	 * EUR/USD exchange price
	 */
	private double eurusd = 0;
	
	/**
	 * API key for service
	 */
	private static String apiKey = "";
	
	/**
	 * {@inherit}
	 */
	public String getName() {
		return PROVIDER_NAME;
	}
	
	/**
	 * Set API key
	 * 
	 * @param apiKey
	 */
	public void setAPI(final String apiKey) {
		this.apiKey = apiKey;
	}
	
	/**
	 * Download EUR/USD currency exchange values if not existed
	 * 
	 * @throws IOException 
	 */
	private void checkExchange() throws IOException {
		if (eurusd == 0) {
			final FxQuote quote = YahooFinance.getFx("EURUSD=X");
			eurusd = quote.getPrice().doubleValue();
		}
	}

	/**
	 * {@inherit}
	 */
	public Stock search(final String symbol) throws ProviderException {
		try {
			checkExchange();
			
			final yahoofinance.Stock stock = YahooFinance.get(symbol.toUpperCase());
			
			if (stock != null && stock.getName() != null) {
				final Stock result = new Stock();
				
				result.setName(stock.getName());
				result.setSymbol(symbol.toUpperCase());
				try {
					result.setActualPrice(stock.getQuote().getPrice().doubleValue());
					result.setCurrency(stock.getCurrency());
					
					if (result.getCurrency().contentEquals("USD")) {
						result.setActualPrice(result.getActualPrice() / eurusd);
						result.setCurrency("EUR");
					}
				} catch (Exception e) {
				}
				
				return result;
			} else {
				return null;
			}
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Returns the actual price
	 * 
	 * @param symbol	The stock symbol
	 * 
	 * @return			Actual price
	 */
	public double getPrice(final String symbol) {
		try {
			final yahoofinance.Stock stock = YahooFinance.get(symbol.toUpperCase());
			
			if (stock != null && stock.getName() != null) {
				return stock.getQuote().getPrice().doubleValue();
			} else {
				return 0;
			}
		} catch (IOException e) {
			return 0;
		}
	}
	
	/**
	 * Returns the historical data
	 * 
	 * @param symbol	The stock symbol
	 * 
	 * @throws ProviderException 
	 */
	public HistoryEntry[] getHistory(final String symbol, final EHistoryPeriod period) throws ProviderException {
		try {
			final Calendar from = Calendar.getInstance();
			final Calendar to = Calendar.getInstance();
			
			switch (period) {
			case HISTORY_1_MONTH:
				from.add(Calendar.MONTH, -1);
				break;
			case HISTORY_3_MONTH:
				from.add(Calendar.MONTH, -3);
				break;
			case HISTORY_6_MONTH:
				from.add(Calendar.MONTH, -6);
				break;
			case HISTORY_1_JAHR:
				from.add(Calendar.YEAR, -1);
				break;
			}

			yahoofinance.Stock stock = YahooFinance.get(symbol.toUpperCase());
			final List<HistoricalQuote> quotes = stock.getHistory(from, to, Interval.DAILY);
			
			final HistoryEntry [] result = new HistoryEntry [quotes.size()];
			/*
			for (int i = 0; i < result.length; i++) {
				result[i] = quotes.get(i).getClose().doubleValue();
				if (stock.getCurrency().contentEquals("USD")) {
					result[i] = result[i] / eurusd;
				}
			}*/
			
			return result;
			
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getExchangeCurse(String currency) {
		return null;
	}
}
