package de.malex.stockdb.financial.providers;

import java.util.Calendar;

/**
 * 
 * @author Alexandr
 *
 */
public class HistoryEntry {
	/**
	 * Close price
	 */
	private double price;
	
	/**
	 * Price date
	 */
	private Calendar date;

	/**
	 * 
	 */
	public HistoryEntry() {
	}
	
	public HistoryEntry(double price, Calendar date) {
		this.price = price;
		this.date = date;
	}
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}
}
