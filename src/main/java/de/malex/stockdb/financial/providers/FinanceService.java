package de.malex.stockdb.financial.providers;

import java.util.ArrayList;
import java.util.List;

import de.malex.stockdb.db.model.Stock;

/**
 * The finance service class
 * 
 * @author Alexandr Mitiaev
 *
 */
public class FinanceService {
	/**
	 * History period values
	 */
	public enum EHistoryPeriod {HISTORY_1_MONTH, HISTORY_3_MONTH, HISTORY_6_MONTH, HISTORY_1_JAHR, HISTORY_3_JAHR, HISTORY_5_JAHR, HISTORY_10_JAHR};
	
	/**
	 * The {@link FinanceService} instance
	 */
	private static FinanceService instance						=		null;
	
	/**
	 * Available {@link IProvider} implementations
	 */
	private static final List<IProvider> providers				=		new ArrayList<IProvider>();
	
	/**
	 * The {@link IProvider} to get financial data
	 */
	private static IProvider provider							=		null;
	
	/**
	 * Private constructor
	 */
	private FinanceService() {
		providers.add(new YahooProvider());
		providers.add(new AlphaVantageProvider());
		providers.add(new WorldTradingDataProvider());
		
		provider = providers.get(0);
	}
	
	/**
	 * Returns the {@link FinanceService} instance
	 * 
	 * @return	The {@link FinanceService}
	 */
	public static FinanceService getInst() {
		if (instance == null) {
			instance = new FinanceService();
		}
		
		return instance;
	}
	
	/**
	 * Returns list of available financial data providers
	 * 
	 * @return	List of {@link IProvider}
	 */
	public List<IProvider> getProviders() {
		return providers;
	}
	
	/**
	 * Returns provider by index, of null if index is out of range
	 * 
	 * @param index	The provider index
	 * 
	 * @return	The {@link IProvider} instance or null
	 */
	public IProvider getProvider(final int index) {
		if (index < providers.size()) {
			return providers.get(index);
		} else {
			return null;
		}
	}
	
	/**
	 * Sets the {@link IProvider} as active
	 * 
	 * @param fin_provider The financial data {@link IProvider}
	 */
	public void setProvider(final IProvider fin_provider) {
		provider = fin_provider;
	}
	
	/**
	 * Returns name of actual financial data provider
	 * 
	 * @return	The {@link IProvider#getName()}
	 */
	public String getProviderName() {
		return provider.getName();
	}
	
	/**
	 * Search the {@link Stock} by given symbol
	 * 
	 * @throws ProviderException 
	 */
	public Stock search(final String symbol) throws ProviderException {
		return provider.search(symbol);
	}
	
	/**
	 * Update actual price field in {@link Stock} instance
	 * 
	 * @param stock	The {@link Stock} instance to update
	 * 
	 * @throws ProviderException 
	 */
	public void updatePrice(final Stock stock) throws ProviderException {
		final Stock s = provider.search(stock.getSymbol());
		
		if (s != null) {
			stock.setActualPrice(s.getActualPrice());
			stock.setCurrency(s.getCurrency());
		}
	}
	
	/**
	 * Update stock history data
	 * 
	 * @param stock	The stock to update
	 * 
	 * @throws ProviderException
	 */
	public void updateHistory(final Stock stock, EHistoryPeriod period) throws ProviderException {
		stock.setHistory(provider.getHistory(stock.getSymbol(), period));
	}
}
