package de.malex.stockdb.db.model;

import java.util.Date;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class buy orders
 * 
 * @author Alexandr Mitiaev
 * 
 */
@Entity
@Table(name = "sell_orders")
public class StockSell
{
	/**
	 * User ID
	 */
	@Id
	@Column(name="id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * User ID
	 */
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "stockId")
	private Stock stock;
	
	/**
	 * Buy price
	 */
	@Column(name = "price")
	private double price;
	
	/**
	 * Buy amount
	 */
	@Column(name = "amount")
	private Integer amount;
	
	/**
	 * Order date
	 */
	@Column(name = "date")
	private Date date;

	/**
	 * Create new {@link StockSell} object
	 */
	public StockSell() {
	}
	
	/**
	 * Returns the {@link #id}
	 * 
	 * @return	The {@link #id} value
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Sets new value of the {@link #id} field
	 * 
	 * @param id	The new value to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * Returns the {@link #price}
	 * 
	 * @return	The {@link #price} value
	 */
	public double getPrice() {
		return price;
	}
	
	/**
	 * Sets new value of the {@link #price} field
	 * 
	 * @param material	The new value to set
	 */
	public void setPrice(final double price) {
		this.price = price;
	}

	/**
	 * Returns the {@link #amount}
	 * 
	 * @return	The {@link #amount} value
	 */
	public Integer getAmount() {
		return amount;
	}
	
	/**
	 * Sets new value of the {@link #amount} field
	 * 
	 * @param distance	The new value to set
	 */
	public void setAmount(final Integer amount) {
		this.amount = amount;
	}

	/**
	 * Returns the {@link #date}
	 * 
	 * @return	The {@link #date} value
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets new value of the {@link #date} field
	 * 
	 * @param user	The new value to set
	 */
	public void setDate(final Date date) {
		this.date = date;
	}
	
	public String getDateStr() {
		if (date != null) {
			return new SimpleDateFormat("dd-MM-yy HH:mm").format(date);
		} else {
			return "-";
		}
	}

	/**
	 * Returns the {@link #stock}
	 * 
	 * @return	The {@link #stock} value
	 */
	public Stock getStock() {
		return stock;
	}

	/**
	 * Sets new value of the {@link #stock} field
	 * 
	 * @param stock	The new value to set
	 */
	public void setStock(final Stock stock) {
		this.stock = stock;
	}
}
