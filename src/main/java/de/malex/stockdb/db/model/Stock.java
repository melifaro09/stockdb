package de.malex.stockdb.db.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import de.malex.stockdb.financial.providers.HistoryEntry;

/**
 * Entity class "Stock"
 * 
 * @author Alexandr Mitiaev
 * 
 */
@Entity
@Table(name = "stocks")
public class Stock
{
	/**
	 * Stock ID
	 */
	@Id
	@Column(name="id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Stock name
	 */
	@Column(name = "name", length=256)
	private String name;
	
	/**
	 * Stock symbol
	 */
	@Column(name = "symbol", length=16)
	private String symbol;
	
	/**
	 * Actual stock price (not a DB-field)
	 */
	@Column(name = "actual_price")
	private Double actualPrice;
	
	/**
	 * Currency name (not a DB-field)
	 */
	@Column(name = "currency", length=3)
	private String currency;
	
	/**
	 * History data
	 */
	transient HistoryEntry [] history;

	/**
	 * List of buyed stocks
	 */
	@OneToMany(mappedBy = "stock",
			   cascade = CascadeType.ALL,
			   orphanRemoval = true)
	private List<StockBuy> buyList = new ArrayList<StockBuy>();
	
	
	/**
	 * List of selled stocks
	 */
	@OneToMany(mappedBy = "stock",
			   cascade = CascadeType.ALL,
			   orphanRemoval = true)
	private List<StockSell> sellList = new ArrayList<StockSell>();

	/**
	 * Create new {@link Stock} object
	 */
	public Stock() {
	}

	/**
	 * Returns the {@link #id}
	 * 
	 * @return	The {@link #id} value
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets new value of the {@link #id} field
	 * 
	 * @param userId	The new value to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Returns the {@link #name}
	 * 
	 * @return	The {@link #name} value
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets new value of the {@link #name} field
	 * 
	 * @param sex	The new value to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Returns the {@link #symbol}
	 * 
	 * @return	The {@link #symbol} value
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * Sets new value of the {@link #symbol} field
	 * 
	 * @param age	The new value to set
	 */
	public void setSymbol(final String symbol) {
		this.symbol = symbol;
	}
	
	/**
	 * Returns the buy orders for this stock
	 * 
	 * @return	The array list of {@link StockBuy}
	 */
	public List<StockBuy> getBuyOrders() {
		return buyList;
	}
	
	/**
	 * Sets the buy orders for this stock
	 * 
	 * @param orders	The order list to set
	 */
	public void setBuyOrders(final List<StockBuy> orders) {
		this.buyList = orders;
	}
	
	/**
	 * Append one buy order to the order list
	 * 
	 * @param order	The {@link StockBuy} order to append
	 */
	public void addBuyOrder(final StockBuy order) {
        buyList.add(order);
        order.setStock(this);
    }
 
	/**
	 * Remove one buy order from the order list
	 * 
	 * @param order The {@link StockBuy} order to remove
	 */
    public void removeBuyOrder(final StockBuy order) {
        buyList.remove(order);
        order.setStock(null);
    }
    
    /**
	 * Returns the sell orders for this stock
	 * 
	 * @return	The array list of {@link StockSell}
	 */
	public List<StockSell> getSellOrders() {
		return sellList;
	}
	
	/**
	 * Sets the sell orders for this stock
	 * 
	 * @param orders	The order list to set
	 */
	public void setSellOrders(final List<StockSell> orders) {
		this.sellList = orders;
	}
	
	/**
	 * Append one sell order to the order list
	 * 
	 * @param order	The {@link StockSell} order to append
	 */
	public void addSellOrder(final StockSell order) {
		if (sellList.indexOf(order) == -1) {
			sellList.add(order);
		}
		
        order.setStock(this);
    }
 
	/**
	 * Remove one sell order from the order list
	 * 
	 * @param order The {@link StockSell} order to remove
	 */
    public void removeSellOrder(final StockSell order) {
    	if (!(sellList.indexOf(order) == -1)) {
    		sellList.remove(order);
    	}
    	
        order.setStock(null);
    }
    
    /**
     * Returns number of selled stocks
     * 
     * @return
     */
    public int getSellCount() {
    	if (sellList != null) {
    		int count = 0;
    		
    		for (final StockSell sellOrder: sellList) {
    			count += sellOrder.getAmount();
    		}
    		
    		return count;
    	} else {
    		return 0;
    	}
    }
    
    /**
     * Returns number of buyed stocks
     * 
     * @return
     */
    public int getBuyCount() {
    	if (buyList != null) {
    		int count = 0;
    		
    		for (final StockBuy buyOrder: buyList) {
    			count += buyOrder.getAmount();
    		}
    		
    		return count;
    	} else {
    		return 0;
    	}
    }
    
    /**
     * Get number for existed stocks
     * 
     * @return Number of existed stocks
     */
    public int getExistedCount() {
    	return getBuyCount() - getSellCount();
    }
    
    /**
     * Returns the price for existed stocks
     * 
     * @return
     */
    public String getTotalPriceStr() {
    	if (getActualPrice() != null) {
    		return String.format("%.2f %s", getExistedCount() * getActualPrice(), getCurrency());
    	} else {
    		return "-";
    	}
    }
    
    /**
     * Returns sell price sum over all time
     * 
     * @return	The total sell price
     */
    public double getSellSum() {
    	if (sellList != null) {
    		double price = 0;
    		
    		for (final StockSell sellOrder: sellList) {
    			price += sellOrder.getPrice() * sellOrder.getAmount();
    		}
    		
    		return price;
    	} else {
    		return 0;
    	}
    }
    
    /**
     * Returns buy price sum over all time
     * 
     * @return	The total buy price
     */
    public double getBuySum() {
    	if (buyList != null) {
    		double price = 0;
    		
    		for (final StockBuy buyOrder: buyList) {
    			price += buyOrder.getPrice() * buyOrder.getAmount();
    		}
    		
    		return price;
    	} else {
    		return 0;
    	}
    }
    
    /**
     * Get profit over all time
     * 
     * @return
     */
    public double getProfit() {
    	return getPLStatistic(0);
    	//return getSellSum() - getBuySum();
    }
    
    public String getProfitStr() {
    	return String.format("%.2f %s", getPLStatistic(0), currency);
    }
    
    /**
     * Returns the {@link #actualPrice}
     * 
     * @return The {@link #actualPrice}
     */
	public Double getActualPrice() {
		if (actualPrice == null) {
			return 0.0;
		} else {
			return actualPrice;
		}
	}
	
	/**
	 * Returns actual price as a string with currency
	 * 
	 * @return	Actual price with a currency
	 */
	public String getActualPriceStr() {
		if (actualPrice == null) {
			return "-";
		} else {
			return String.format("%.2f %s", actualPrice, currency);
		}
	}

	/**
	 * Sets the new value for {@link #actualPrice}
	 * 
	 * @param actualPrice New value for {@link #actualPrice}
	 */
	public void setActualPrice(final Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	
	/**
	 * Returns the {@link #currency}
	 * 
	 * @return	The {@link #currecy}
	 */
	public String getCurrency() {
		if (currency == null) {
			currency = "EUR";
		}

		return currency;
	}

	/**
	 * Sets the new value for {@link #currency}
	 * 
	 * @param currency New value for {@link #currency}
	 */
	public void setCurrency(final String currency) {
		this.currency = currency;
	}
	
	/**
	 * Returns stock name
	 */
	@Override
	public String toString() {
		return name;
	}
	
	/**
	 * Returns the {@link #history}
	 * 
	 * @return The {@link #history}
	 */
	public HistoryEntry[] getHistory() {
		return history;
	}
	
	/**
	 * Sets the {@link #history}
	 * 
	 * @param history	The new value for the {@link #history}
	 */
	public void setHistory(HistoryEntry [] history) {
		this.history = history;
	}
	
	/**
	 * Average sell price
	 * @return
	 */
	public String getAvgSellPrice() {
		if (sellList != null && sellList.size() > 0) {
			double sum = 0;
			int count = 0;
			for (int i = 0; i < sellList.size(); i++) {
				count += sellList.get(i).getAmount();
				sum += sellList.get(i).getPrice() * sellList.get(i).getAmount();
			}
			
			sum /= count;
		
			return String.format("%.2f %s", sum, currency);
		} else {
			return "-";
		}
	}
	
	/**
	 * Returns the Profit/Lost statistic since "days"
	 * 
	 * @param days	The days to calculate statistic
	 * @return
	 */
	public double getPLStatistic(int days) {
		if (getActualPrice() == null) {
			return 0;
		}
		
		double lost, profit;
		double buyAvgPrice = 0, sellAvgPrice = 0;
		int buyCount = 0, sellCount = 0;
		final Calendar since = Calendar.getInstance();
		
		since.add(Calendar.DATE, -days);
		
		for (final StockBuy buyOrder: getBuyOrders()) {
			if (buyOrder.getDate().after(since.getTime()) || days == 0) {
				buyCount += buyOrder.getAmount();
				buyAvgPrice += buyOrder.getPrice() * buyOrder.getAmount();
			}
		}
		if (buyCount == 0) {
			return 0;
		}
		buyAvgPrice /= buyCount;
		
		for (final StockSell sellOrder: getSellOrders()) {
			if (sellOrder.getDate().after(since.getTime()) || days == 0) {
				sellCount += sellOrder.getAmount();
				sellAvgPrice += sellOrder.getPrice() * sellOrder.getAmount();
			}
		}
		if (sellCount > 0) {
			sellAvgPrice /= sellCount;
		}
		
		if (buyCount > sellCount) {
			lost = buyCount * buyAvgPrice;
			profit = sellCount * sellAvgPrice + (buyCount - sellCount) * getActualPrice();
		} else {
			lost = buyCount * buyAvgPrice;
			profit = buyCount * sellAvgPrice;
		}
		
		return profit - lost;
	}
}
