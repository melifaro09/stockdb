package de.malex.stockdb.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import de.malex.stockdb.db.model.Stock;
import de.malex.stockdb.db.model.StockBuy;
import de.malex.stockdb.db.model.StockSell;

/**
 * Facade for access to database
 * 
 * @author Alexandr Mitiaev
 */
public class DBFacade {
	/**
	 * The current {@link Session}
	 */
	public static Session session = null;
	
	/**
	 * Open new database session
	 * 
	 * @throws DatabaseException 
	 */
	public static void openSession() throws DatabaseException {
		if (session == null) {
			try {
				final Database db = Database.getInstance();
				db.openSession();
				session = db.getSession();
			} catch (Exception e) {
				throw new DatabaseException("Database open error: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Close current database session
	 * 
	 * @throws DatabaseException 
	 */
	public static void closeSession() throws DatabaseException {
		final Database db = Database.getInstance();
			
		if (session != null) {
			try {
				db.closeSession();
			} finally {
				session = null;
			}
		}
	}
	
	/**
	 * Check session state and open if needed
	 * 
	 * @throws DatabaseException
	 */
	private static void checkSession() throws DatabaseException {
		if (session == null) {
			openSession();
		}
	}
	
	/**
	 * Remove the {@link Stock} entry from the database
	 * 
	 * @param stock the {@link Stock} to delete
	 * 
	 * @throws DatabaseException 
	 */
	public static void deleteStock(final Stock stock) throws DatabaseException {
		checkSession();
		
		final Transaction transaction = session.beginTransaction();
		
		try {
			session.delete(stock);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw new DatabaseException("Stock delete error: " + e.getMessage());
		}
	}
	
	/**
	 * Return list of {@link Stock}s from the database
	 * 
	 * @return A list of the {@link Stock} entitys
	 * 
	 * @throws DatabaseException 
	 */
	@SuppressWarnings("unchecked")
	public static List<Stock> getAllStocks() throws DatabaseException {
		checkSession();

		return session.createCriteria(Stock.class).list();
	}
	
	/**
	 * Get stock by ID
	 * 
	 * @param id	The ID of a stock
	 * 
	 * @return		The {@link Stock}
	 */
	public static Stock getStockById(final long id) {
		return (Stock) session.get(Stock.class, id);
	}
	
	/**
	 * Get stock by symbol
	 * 
	 * @param id	The symbol of a stock
	 * 
	 * @return		The {@link Stock}
	 */
	public static Stock getStockBySymbol(final String symbol) {
		final Criteria crit = session.createCriteria(Stock.class);
		crit.add(Restrictions.eq("symbol", symbol));
		
		@SuppressWarnings("unchecked")
		final List<Stock> results = crit.list();
		
		if (results.size() == 0) {
			return null;
		} else {
			return results.get(0);
		}
	}
	
	/**
	 * Save or update the {@link Stock} in database
	 * 
	 * @param stock	The {@link Stock} instance
	 * @throws DatabaseException 
	 */
	public static void saveStock(final Stock stock) throws DatabaseException {
		checkSession();

		final Transaction transaction = session.beginTransaction();

		try {
			session.saveOrUpdate(stock);
			session.flush();
				
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw new DatabaseException("Stock saving error: " + e.getMessage());
		}
	}
	
	/**
	 * Save or update the {@link Stock} in database
	 * 
	 * @param stock	The {@link Stock} instance
	 * @throws DatabaseException 
	 */
	public static void saveStockBuy(final StockBuy stockBuy) throws DatabaseException {
		checkSession();

		final Transaction transaction = session.beginTransaction();

		try {
			session.saveOrUpdate(stockBuy);
			session.flush();
				
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw new DatabaseException("Stock buy saving error: " + e.getMessage());
		}
	}
	
	/**
	 * Save or update the {@link Stock} in database
	 * 
	 * @param stock	The {@link Stock} instance
	 * @throws DatabaseException 
	 */
	public static void saveStockSell(final StockSell stockSell) throws DatabaseException {
		checkSession();

		final Transaction transaction = session.beginTransaction();

		try {
			session.saveOrUpdate(stockSell);
			session.flush();
				
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
			throw new DatabaseException("Stock sell saving error: " + e.getMessage());
		}
	}
	
	/**
	 * Fill the {@link Experiment} object with a input/output data
	 * 
	 * @param experiment the {@link Experiment} to fill
	 */
	/*
	public static void getExperimentData(Experiment experiment) {
		if (experiment.getSpsValues().size() > 0)
			experiment.ioInitialize();
		else
			Hibernate.initialize(experiment.getSpsValues());
	}
	*/
}
