/**
 * 
 */
package de.malex.stockdb.db;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistryBuilder;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Singleton-object for access to database session
 * 
 * @author Alexandr Mitiaev
 */
public class Database
{
	/**
	 * The instance of the {@link Database}
	 */
	private static Database instance = null;
	
	/**
	 * The {@link SessionFactory} to use
	 */
    private static SessionFactory sessionFactory = null;
    
    /**
     * The {@link Session} to use
     */
    private static Session session = null;
    
    /**
     * Private constructor for {@link Database} class
     * @throws DatabaseException 
     */
    private Database() throws DatabaseException {
    	configureSessionFactory();
    }
    
    /**
     * Returns the instance of the {@link Database} class
     * 
     * @return instance of the {@link Database} class
     * @throws DatabaseException 
     */
    public static Database getInstance() throws DatabaseException {
    	if (instance == null)
    		instance = new Database();
    	
    	return instance;
    }
    
    final String config = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
    		"<!DOCTYPE hibernate-configuration PUBLIC \"-//Hibernate/Hibernate Configuration DTD 3.0//EN\" \r\n" + 
    		"\"http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd\">\r\n" + 
    		" \r\n" + 
    		"<hibernate-configuration>\r\n" + 
    		"    <session-factory>\r\n" + 
    		"        <property name=\"show_sql\">false</property>\r\n" + 
    		"        <property name=\"format_sql\">true</property>\r\n" + 
    		"        <property name=\"dialect\">org.hibernate.dialect.SQLiteDialect</property>\r\n" + 
    		"        <property name=\"connection.driver_class\">org.sqlite.JDBC</property>\r\n" + 
    		"        <property name=\"connection.url\">jdbc:sqlite:data.db</property>\r\n" + 
    		"        <property name=\"connection.username\"></property>\r\n" + 
    		"        <property name=\"connection.password\"></property>\r\n" + 
    		"         \r\n" + 
    		"        <!-- <property name=\"hibernate.hbm2ddl.auto\">create</property> -->\r\n" + 
    		"        <property name=\"hibernate.hbm2ddl.auto\">update</property>\r\n" + 
    		"\r\n" + 
    		"        <!-- Common -->\r\n" + 
    		"        <mapping class=\"de.malex.stockdb.db.model.Stock\" />\r\n" + 
    		"		<mapping class=\"de.malex.stockdb.db.model.StockBuy\" />\r\n" + 
    		"		<mapping class=\"de.malex.stockdb.db.model.StockSell\" />\r\n" + 
    		"    </session-factory>\r\n" + 
    		"</hibernate-configuration>\r\n";
    
    /**
     * Configure session factory
     * @throws DatabaseException 
     * @throws ParserConfigurationException 
     * @throws IOException 
     * @throws SAXException 
     * @throws HibernateException 
     */
    private void configureSessionFactory() throws DatabaseException {
    	try {
    		Configuration configuration = new Configuration();
    		//configuration.configure("hibernate.cfg.xml");
    	
    		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    		InputSource is = new InputSource();
    		is.setCharacterStream(new StringReader(config));
    			
    		Properties properties = configuration.configure(db.parse(is)).getProperties();
         
        	//Properties properties = configuration.getProperties();
         
    		sessionFactory = configuration.buildSessionFactory(new ServiceRegistryBuilder().applySettings(properties).buildServiceRegistry());
    	} catch (Exception e) {
    		throw new DatabaseException("Error by loading Hibernate configuration");
    	}
    }
    
    /**
     * Configure database with a new .db file and open
     * session
     * 
     * @param dbfile		The .db file to load
     */
    public void reConfigureDB(final String dbfile) {
    	boolean reopen = false;
    	
    	if (session != null) {
    		session.close();
    		session = null;
    		reopen = true;
    	}
    	
    	Configuration config = new Configuration();
    	config.configure("hibernate.cfg.xml");
    	Properties props = config.getProperties();
    	props.setProperty("hibernate.connection.url", String.format("jdbc:sqlite:%s", dbfile));
    	sessionFactory = config.buildSessionFactory(new ServiceRegistryBuilder().applySettings(props).buildServiceRegistry());
    	
    	if (reopen) {
    		openSession();
    	}
    }
    
    /**
     * Open new session
     */
    public void openSession() {
    	if (session != null)
    		closeSession();
    	
    	session = sessionFactory.openSession();
    }
    
    /**
     * Close current session
     */
    public void closeSession() {
    	if (sessionFactory != null) {
    		sessionFactory.close();
    	}
    	
    	//if (session != null) {
    	//	session.close();
    	//}

    	sessionFactory = null;
    	session = null;
    }
    
    /**
     * Returns the {@link Session} object
     * 
     * @return the {@link Session} object
     */
    public Session getSession() {
    	return session;
    }
}
