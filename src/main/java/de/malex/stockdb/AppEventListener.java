package de.malex.stockdb;

/**
 * Application event listener interface
 * 
 * @author Alexandr Mitiaev
 */
public interface AppEventListener {
	/**
	 * Application closing event
	 */
	public void onAppClose();
}
