# StockDB

## Таблицы БД

### Stock

 **Field**   | **Type**               | **Description**                           |
-------------|------------------------|-------------------------------------------|
 id          | Long                   | Stock ID                                  |
 name        | String(256)            | Stock name                                |
 symbol      | String(16)             | Stock symbol                              |
 actualPrice | Double (virtual field) | Actual price from a finance provider      |
 currency    | String (virtual field) | Price currency                            |
 buyList     | List<StockBuy>         | List of a StockBuy-entries for the stock  |
 sellList    | List<StockSell>        | List of a StockSell-entries for the stock |
 

### StockBuy
 **Field** | **Type**   | **Description** |
-----------|------------|-----------------|
 id        | Long       | Order ID        |
 stock     | Stock      | Stock           |
 price     | Double     | Buy price       |
 date      | Date       | Buy date        |

### StockSell
 **Field** | **Type**   | **Description** |
-----------|------------|-----------------|
 id        | Long       | Order ID        |
 stock     | Stock      | Stock           |
 price     | Double     | Buy price       |
 date      | Date       | Buy date        |
 
 ## Классы для работы с БД
 
 ### DBFacade
 
 Класс обеспечивает доступ к функциональности базы данных: поиск, сохранение, удаление записей.
 
 #### Методы DBFacade
 
- void ***deleteStock***(Stock stock); - удалить акцию из базы данных

- void ***saveStock***(Stock stock); - добавить или обновить акцию в базе данных

- void ***getStockById***(long id); - поиск акции по ID

- List<Stock> ***getAllStocks***(); - получение всех акций из базы данных